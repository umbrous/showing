/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'jquery/ui'
], function($) {
    "use strict";

    $.widget('ot.openOrder', {


         _create: function () {
             var self = this;
             $(self.element).on('click', '.table-arr', function(event) {
                 var $this = $(this).parent().parent();
                 if (!$this.hasClass('active')) {
                     $this.addClass('active');
                     $('#' + $this.attr('data-title')).show();
                 }
                 else{
                     $this.removeClass('active');
                     $('#' + $this.attr('data-title')).hide();
                 }
             });
         }
    });

    return $.ot.openOrder;
});