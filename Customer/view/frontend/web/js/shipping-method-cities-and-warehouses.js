/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/translate',
    'jquery/ui',
    'select2'
], function($, $t) {
    "use strict";

    $.widget('ot.shippingMethodCitiesAndWarehouses', {

        options: {
            newposhta_newposhta: '/novaposhta/ajax/cities',
            newposhta_newposhta_warehouses: '/novaposhta/ajax/warehouses'
        },

         _create: function () {
             this._bindChangeShippingMethod();
             this.shippingMethod = '';
             this._init_shipping_cities(this);
             this._getShippingWarehouses(this.options.defaultCity);
         },

        _init_shipping_cities: function (form) {
            form.shippingMethod = $(form.element).find('select[name="shippingMethod"]').val();
            form._getShippingMethodCities();
            form._fillWarehousesInput(false);
            $('select').select2({
                minimumResultsForSearch: -1
            });
        },

         _getShippingMethodCities : function () {
             var form = this;
             if(form.shippingMethod){
                 $.ajax({
                     url: form.options[form.shippingMethod],
                     type: 'post',
                     dataType: 'JSON',
                     beforeSend: function () {
                       form._showPreloader();
                     },
                     success: function(res) {
                         form._fillCitiesInput(res);
                         form._bindChangeShippingCities();
                         form._hidePreloader()
                     }
                 });
             } else {
                 form._fillCitiesInput(false);
             }
         },

        _getShippingWarehouses : function (city) {
            var form = this;
            if(form.shippingMethod){
                $.ajax({
                    url: form.options[form.shippingMethod+'_warehouses'],
                    type: 'post',
                    data: 'city=' + city,
                    dataType: 'JSON',
                    beforeSend: function () {
                        form._showPreloader();
                    },
                    success: function(res) {
                        form._fillWarehousesInput(res);
                        form._hidePreloader()
                    }
                });
            } else {
                form._fillWarehousesInput(false);
            }
        },

        _bindChangeShippingMethod: function() {
            var form = this;
            var shippingMethod = $(form.element).find('select[name="shippingMethod"]');
            shippingMethod.on('change', function () {
                form._init_shipping_cities(form);
            })
        },

        _bindChangeShippingCities: function() {
            var form = this;
            var shippingCities = $(form.element).find('select[name="shippingCities"]');
            shippingCities.on('change', function () {
                form._getShippingWarehouses(shippingCities.val())
            })
        },

        _fillCitiesInput: function (data) {
            var form = this;
            var shippingCities = $(form.element).find('select[name="shippingCities"]');
            shippingCities.html('<option value="">'+ $t('Select city...') +'</option>');
            if (data) {
                for (var i = 0; i<data.length; i++){
                    if(data[i]['id'] === form.options.defaultCity) {
                        shippingCities.append('<option value="'+ data[i]['id'] +'" selected>'+ data[i]['name'] +'</option>');
                        form.options.defaultCity = '';
                    } else {
                        shippingCities.append('<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>');
                    }
                }
            }
            shippingCities.select2();
        },

        _fillWarehousesInput: function (data) {
            var form = this;
            var shippingWarehouses = $(form.element).find('select[name="shippingWarehouses"]');
            shippingWarehouses.html('<option value="">' + $t('Select warehouse...') +'</option>');
            if (data) {
                for (var i = 0; i<data.length; i++){
                    if(data[i]['id'] === form.options.defaultWarehouse) {
                        shippingWarehouses.append('<option value="'+ data[i]['id'] +'" selected>'+ data[i]['name'] +'</option>');
                        form.options.defaultWarehouse = '';
                    } else {
                        shippingWarehouses.append('<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>')
                    }
                }
            }
            shippingWarehouses.select2();
        },

        _showPreloader: function () {
            var form = this;
            $(form.element).find('#submit-account-settings div.panel-body').hide();
            $(form.element).find('#submit-account-settings div.cs-loader').show();
        },

        _hidePreloader: function () {
            var form = this;
            $(form.element).find('#submit-account-settings div.panel-body').show();
            $(form.element).find('#submit-account-settings div.cs-loader').hide();
        }

    });

    return $.ot.shippingMethodCitiesAndWarehouses;
});