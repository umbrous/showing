define([
    'otProductListAbstract',
    'ko',
    'jquery',
    'mage/translate',
    'otLoader',
    'cartStorage',
    'msgStorage'
], function (otProductListAbstract, ko, $, $t, loading, cartStorage, msgStorage) {
    'use strict';

    var self, sku, qty;

    return otProductListAbstract.extend({
        defaults: {
            template: 'OutsourcingTeam_Customer/add'
        },

        cart: ko.observableArray(),
        msg: ko.observableArray(),

        initialize: function () {
            self  = this;
            self._super();
            self.msg = msgStorage.msg;

            self.cart = cartStorage.cart;

            return self;
        },

        loadCartAfterAdd: function () {
            if(self.jsonUrl !== false){
                loading.setLoading('loadCartAfterAdd');

                $.getJSON(self.jsonUrl, function(data) {
                    self.cart(data);
                    loading.setLoaded('loadCartAfterAdd');
                });
            }
        },

        addBySKU: function (target, e) {
            sku = $(e.currentTarget).siblings('input[name="sku"]').val();
            qty = $(e.currentTarget).siblings().find('input[name="add-qty"]').val();
            $.ajax({
                type: "POST",
                url: self.addUrl,
                data: 'qty=' + qty + '&sku=' + sku + '&form_key=' + $('input[name="form_key"]').val(),
                dataType: 'json',
                beforeSend: function () {
                    loading.setLoading('addBySKU');
                    $(e.currentTarget).html('<i class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></i>');
                },
                success: function (data) {
                    self.msg(data);
                    self.loadCartAfterAdd();
                    loading.setLoaded('addBySKU');
                    $(e.currentTarget).html('<i class="fa fa-plus" aria-hidden="true"></i>');
                },
                error: function (data) {
                    loading.setLoaded('addBySKU');
                    self.msg($t('Order not updated!'), 'error');
                    $(e.currentTarget).html('<i class="fa fa-plus" aria-hidden="true"></i>');
                }
            });
        }

    });
});
