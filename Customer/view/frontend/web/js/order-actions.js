define([
    'uiComponent',
    'ko',
    'jquery',
    'otLoader',
    'Magento_Customer/js/customer-data',
    'mage/translate',
    'Magento_Ui/js/modal/confirm',
    'msgStorage'
], function (Component, ko, $, loader, customerData, $t, confirm, msgStorage) {
    'use strict';

    var newOrder, isCheckoutCartHasProducts = function () {
        var cart = customerData.get('cart');
        if (cart().summary_count !== 0){
            return true;
        }
        return false;
    };

    var self;

    return Component.extend({
        defaults: {
            template: 'OutsourcingTeam_Customer/orderactions'
        },

        loading: ko.observableArray([]),
        msg: ko.observableArray(),

        initialize: function () {
            self  = this;
            self._super();
            self.loading = loader.loading;
            self.msg = msgStorage.msg;

            return this;
        },

        buttonDelete: function () {
            var self  = this;

            switch(self.status) {
                case 'saved':
                    return true;
                    break;
                case 'canceled':
                    return true;
                    break;
                default:
                    return false;
            }
        },
        
        buttonReserve: function () {
            var self  = this;

            if(self.status === 'saved') {
                return true;
            }

            return false;
        },

        buttonProcess: function () {
            var self  = this;

            switch(self.status) {
                case 'saved':
                    return true;
                    break;
                case 'holded':
                    return true;
                    break;
                default:
                    return false;
            }
        },

        buttonAddToCurrent: function () {
           return isCheckoutCartHasProducts();
        },

        buttonCancel: function () {
            var self  = this;

            if(self.status === 'holded') {
                return true;
            }

            return false;
        },

        reorder: function (target, e) {
            newOrder = $(e.currentTarget).attr('data-new');

            if(newOrder === 'true' && isCheckoutCartHasProducts() === true){
                confirm({
                    content: $t('This action will remove all products from current order!'),
                    actions: {

                        /** @inheritdoc */
                        confirm: function () {
                            processReorder()
                        }
                    }
                });
            } else {
                processReorder()
            }

            function processReorder() {
                $.ajax({
                    type: "POST",
                    url: $(e.currentTarget).attr('data-href'),
                    data: '&new=' + newOrder ,
                    dataType: 'json',
                    beforeSend: function () {
                        loader.setLoading('reorder');
                    },
                    success: function (data) {
                        self.msg(data['mess'], data['type']);

                        loader.setLoaded('reorder');
                    },
                    error: function () {
                        loader.setLoaded('reorder');
                        self.msg($t('Order not reordered!'), 'error');
                        $(e.currentTarget).html('<span class="fa fa-refresh" aria-hidden="true"></span>');
                    }
                });
            }
        }
        
    });
});
