define(
    ['ko'],
    function (ko) {
        'use strict';
        var cart = ko.observableArray([]);

        return {
            cart: cart
        };
    }
);