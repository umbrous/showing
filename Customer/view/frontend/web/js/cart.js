define([
    'otProductListAbstract',
    'ko',
    'jquery',
    'mage/translate',
    'otLoader',
    'cartStorage',
    'msgStorage'
], function (otProductListAbstract, ko, $, $t, loading, cartStorage, msgStorage) {
    'use strict';

    var selector, self;

    return otProductListAbstract.extend({
        defaults: {
            template: 'OutsourcingTeam_Customer/cart'
        },

        cart: ko.observableArray(),
        msg: ko.observableArray(),

        initialize: function () {
            self  = this;
            self._super();

            self.cart = cartStorage.cart;
            self.msg = msgStorage.msg;

            self.loadCart();

            return self;
        },

        loadCart: function () {
            loading.setLoading('loadCart');

            $.getJSON(self.jsonUrl, function(data) {
                if(data === false){
                    location.reload();
                } else {
                    self.cart(data);
                    loading.setLoaded('loadCart');
                }
            });
        },

        canManageItems: function () {
            switch(self.status) {
                case 'saved':
                    return true;
                    break;
                case 'holded':
                    return true;
                    break;
                default:
                    return false;
            }
        },
        
        deleteItem: function (target, e) {
            $.ajax({
                type: "POST",
                url: $(e.currentTarget).attr('data-href'),
                dataType: 'json',
                beforeSend: function () {
                    loading.setLoading('deleteItem');
                    $(e.currentTarget).html('<span class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></span>');
                },
                success: function (data) {
                    self.msg(data);
                    self.loadCart();
                    loading.setLoaded('deleteItem');
                },
                error: function (data) {
                    loading.setLoaded('deleteItem');
                    self.msg($t('Order not updated!'), 'error');
                    $(e.currentTarget).html('<span class="fa fa-times" aria-hidden="true"></span>');
                }
            });
        },

        updateItem: function (target, e) {
            selector = $(e.currentTarget).siblings().find('input[type="text"]');
            $.ajax({
                type: "POST",
                url: selector.attr('data-update-url'),
                data: '&new_qty=' + selector.val() ,
                dataType: 'json',
                beforeSend: function () {
                    loading.setLoading('updateItem');
                    $(e.currentTarget).html('<span class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></span>');
                },
                success: function (data) {
                    self.msg(data);

                    self.loadCart();

                    loading.setLoaded('updateItem');
                },
                error: function (data) {
                    loading.setLoaded('updateItem');
                    self.msg($t('Order not updated!'), 'error');
                    $(e.currentTarget).html('<span class="fa fa-refresh" aria-hidden="true"></span>');
                }
            });
        }

    });
});
