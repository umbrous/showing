/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            shippingMethodCitiesAndWarehouses: 'OutsourcingTeam_Customer/js/shipping-method-cities-and-warehouses',
            otOpenOrder: 'OutsourcingTeam_Customer/js/open-order-widget',
            otOrderCart: 'OutsourcingTeam_Customer/js/cart',
            otOrderActions: 'OutsourcingTeam_Customer/js/order-actions',
            otAddBySku: 'OutsourcingTeam_Customer/js/addBySku',
            cartStorage: 'OutsourcingTeam_Customer/js/data-storage/cart'
        }
    }
};
