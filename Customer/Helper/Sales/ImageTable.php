<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Helper\Sales;


use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Helper\Image;

class ImageTable
{
    protected $imageHelper;

    /**
     * @param Image $imageHelper
     */
    public function __construct(
        Image $imageHelper
    ) {
        $this->imageHelper = $imageHelper;
    }

    /**
     * @param $product ProductInterface
     * @return array
     */
    public function getTableProductImage(ProductInterface $product)
    {
        return [
            'src' => $this->imageHelper->init($product, 'product_page_image_small')->setImageFile($product->getFile())->resize(200, 200)->getUrl(),
            'alt' => $product->getName()
        ];
    }
}