<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorInterface;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class OrderBehaviorStrategyTest extends TestCase
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $model;
    /**
     * @var OrderBehaviorInterface
     */
    protected $orderBehaviorInterfaceMock;

    /**
     * @var OrderInterface
     */
    protected $orderMock;

    /**
     * @var OrderInterface
     */
    protected $wrongOrderMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;

    protected function setUp()
    {
        $this->orderBehaviorInterfaceMock = $this
            ->getMockBuilder(OrderBehaviorInterface::class)
            ->getMockForAbstractClass();
        $this->orderBehaviorInterfaceMock->expects(
            $this->any()
        )->method('getStatusCode')
            ->willReturn('some status');

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();
        $this->orderMock->expects(
            $this->any()
        )->method('getStatus')
            ->willReturn('some status');
        $this->orderMock->expects(
            $this->any()
        )->method('getItems')
            ->willReturn([$this->orderItemMock]);

        $this->wrongOrderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $this->wrongOrderMock->expects(
            $this->any()
        )->method('getStatus')
            ->willReturn('some wrong status');

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            OrderBehaviorStrategy::class,
            []
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy::setBehavior()
     */
    public function testSetBehaviorConcreteFactory()
    {
        $this->model->setOrder($this->orderMock);
        $this->model->setBehavior($this->orderBehaviorInterfaceMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy::setBehavior()
     */
    public function testSetBehaviorOrderWithWrongStatusConcreteFactory()
    {
        $this->model->setOrder($this->wrongOrderMock);
        $this->expectException(WrongBehaviorException::class);
        $this->model->setBehavior($this->orderBehaviorInterfaceMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy::setBehavior()
     */
    public function testSetBehaviorOrderNotSetConcreteFactory()
    {
        $this->expectException(\LogicException::class);
        $this->model->setBehavior($this->orderBehaviorInterfaceMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy::changeQty()
     */
    public function testChangeQty()
    {
        $this->model->setOrder($this->orderMock);
        $this->model->setBehavior($this->orderBehaviorInterfaceMock);

        $this->orderItemMock->expects(
            $this->any()
        )->method('getItemId')
            ->willReturn(1);

        $this->model->changeQty(1,1);
        $this->expectException(NoSuchEntityException::class);
        $this->model->changeQty(1,5);
    }
}
