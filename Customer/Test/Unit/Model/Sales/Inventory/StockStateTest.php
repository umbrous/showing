<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Inventory;

use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\Behavior\OutOfStockException;
use OutsourcingTeam\Customer\Model\Sales\Inventory\StockState;
use PHPUnit\Framework\TestCase;

class StockStateTest extends TestCase
{
    /**
     * @var StockState
     */
    protected $model;

    /**
     * @var OrderInterface
     */
    protected $orderMock;

    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistryMock;
    /**
     * @var StockItemInterface
     */
    protected $stockItemMock;
    /**
     * @var string
     */
    protected $sku = 'sku';

    protected function setUp()
    {
        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->stockRegistryMock = $this
            ->getMockBuilder(StockRegistryInterface::class)
            ->getMockForAbstractClass();

        $this->stockItemMock = $this
            ->getMockBuilder(StockItemInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock->expects(
            $this->any()
        )->method('getItems')
            ->willReturn([$this->orderItemMock]);

        $this->orderItemMock->expects(
            $this->any()
        )->method('getSku')
            ->willReturn($this->sku);

        $this->stockRegistryMock->expects(
            $this->any()
        )->method('getStockItemBySku')
            ->with($this->sku)
            ->willReturn($this->stockItemMock);

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            StockState::class,
            [
                'stockRegistry' => $this->stockRegistryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\StockState::subtractItemsFromStockByOrder()
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\StockState::subtractItem()
     */
    public function testSubtractItemsFromStockByOrder(){
        $stockQty = 100;
        $orderQty = 45;

        $this->stockItemMock->expects(
            $this->once()
        )->method('getQty')
            ->willReturn($stockQty);

        $this->orderItemMock->expects(
            $this->once()
        )->method('getQtyOrdered')
            ->willReturn($orderQty);

        $this->stockItemMock->expects(
            $this->once()
        )->method('setQty')
            ->with(55);

        $this->stockRegistryMock->expects(
            $this->once()
        )->method('updateStockItemBySku')
            ->with($this->sku, $this->stockItemMock);

        $this->model->subtractItemsFromStockByOrder($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\StockState::subtractItemsFromStockByOrder()
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\StockState::subtractItem()
     */
    public function testSubtractItemsFromStockByOrderWithOutOfStockException(){
        $stockQty = 30;
        $orderQty = 45;

        $this->stockItemMock->expects(
            $this->once()
        )->method('getQty')
            ->willReturn($stockQty);

        $this->orderItemMock->expects(
            $this->once()
        )->method('getQtyOrdered')
            ->willReturn($orderQty);

        $this->stockItemMock->expects(
            $this->never()
        )->method('setQty');

        $this->stockRegistryMock->expects(
            $this->never()
        )->method('updateStockItemBySku');

        $this->expectException(OutOfStockException::class);

        $this->model->subtractItemsFromStockByOrder($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\StockState::addItemsToStockByOrder()
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\StockState::addItem()
     */
    public function testAddItemToStockByOrder(){
        $stockQty = 100;
        $orderQty = 45;

        $this->stockItemMock->expects(
            $this->once()
        )->method('getQty')
            ->willReturn($stockQty);

        $this->orderItemMock->expects(
            $this->once()
        )->method('getQtyOrdered')
            ->willReturn($orderQty);

        $this->stockItemMock->expects(
            $this->once()
        )->method('setQty')
            ->with(145);

        $this->stockRegistryMock->expects(
            $this->once()
        )->method('updateStockItemBySku')
            ->with($this->sku, $this->stockItemMock);

        $this->model->addItemsToStockByOrder($this->orderMock);
    }
}
