<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Inventory;

use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator;
use PHPUnit\Framework\TestCase;

class ProductQtyValidatorTest extends TestCase
{
    /**
     * @var ProductQtyValidator
     */
    protected $model;

    /**
     * @var OrderInterface
     */
    protected $orderMock;

    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;
    /**
     * @var StockStateInterface
     */
    protected $stockStateMock;

    protected function setUp()
    {
        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->stockStateMock = $this
            ->getMockBuilder(StockStateInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock->expects(
            $this->any()
        )->method('getItems')
            ->willReturn([$this->orderItemMock]);

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            ProductQtyValidator::class,
            [
                'stockState' => $this->stockStateMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator::checkOrderQtyBeforeStockChange()
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator::checkQtyByItem()
     */
    public function testCheckOrderQtyBeforeStockChangeWrongQTY(){
        $qtyOrdered = 5;

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getQtyOrdered')
            ->willReturn($qtyOrdered);

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getProductId')
            ->willReturn(1);

        $this->stockStateMock->expects(
            $this->any()
        )->method('checkQty')
            ->with(1, $qtyOrdered)
            ->willReturn(false);

        $this->assertFalse($this->model->checkOrderQtyBeforeStockChange($this->orderMock));
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator::checkOrderQtyBeforeStockChange()
     * @covers \OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator::checkQtyByItem()
     */
    public function testCheckOrderQtyBeforeStockChangeCorrectQty(){
        $qtyOrdered = 5;

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getQtyOrdered')
            ->willReturn($qtyOrdered);

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getProductId')
            ->willReturn(1);

        $this->stockStateMock->expects(
            $this->any()
        )->method('checkQty')
            ->with(1, $qtyOrdered)
            ->willReturn(true);

        $this->assertTrue($this->model->checkOrderQtyBeforeStockChange($this->orderMock));
    }
}
