<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use OutsourcingTeam\Customer\Model\Sales\OrderShippingAddressContext;
use PHPUnit\Framework\TestCase;

class OrderShippingAddressContextTest extends TestCase
{
    /**
     * @var OrderShippingAddressContext
     */
    protected $model;
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehaviorStrategyMock;

    protected function setUp()
    {
        $this->orderBehaviorStrategyMock = $this
            ->getMockBuilder(OrderBehaviorStrategy::class)
            ->setMethods(['initOrder', 'setBehavior', 'changeAddress', 'setComment'])
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            OrderShippingAddressContext::class,
            [
                'orderBehavior' => $this->orderBehaviorStrategyMock
            ]
        );
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesAllowedConcreteStrategiesDataProvider()
    {
        return [
            [
                OrderBehaviorStrategy::STATUS_SAVED,
                'shipping',
                ''
            ],
            [
                OrderBehaviorStrategy::STATUS_HOLDED,
                'shipping',
                'Some comment'
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\OrderShippingAddressContext::change()
     * @param $status
     * @param $shippingMethod
     * @param $comment
     * @dataProvider orderIdsAndStatusesAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsContextForAllowedConcreteStrategies($status, $shippingMethod, $comment)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('changeAddress')
            ->with($shippingMethod);

        if ($comment){
            $this->orderBehaviorStrategyMock->expects(
                $this->once()
            )->method('setComment')
                ->with($comment);
        }

        $this->model->change(1, $shippingMethod, $comment);
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider()
    {
        return [
            [
                OrderBehaviorStrategy::STATUS_CANCELED,
                'shipping',
                ''
            ],
            [
                OrderBehaviorStrategy::STATUS_COMPLETED,
                'shipping',
                ''
            ],
            [
                OrderBehaviorStrategy::STATUS_PENDING,
                'shipping',
                'Some comment'
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\OrderShippingAddressContext::change()
     * @param $status
     * @param $shippingMethod
     * @param $comment
     * @dataProvider orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsExceptionIfNotAllowedConcreteStrategy($status, $shippingMethod, $comment)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('changeAddress');

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('setComment');

        $this->expectException(WrongBehaviorException::class);

        $this->model->change(1, $shippingMethod, $comment);
    }
}
