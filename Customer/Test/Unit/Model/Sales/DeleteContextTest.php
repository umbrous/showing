<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;
use OutsourcingTeam\Customer\Model\Sales\DeleteContext;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class DeleteContextTest extends TestCase
{
    /**
     * @var DeleteContext
     */
    protected $model;
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehaviorStrategyMock;

    protected function setUp()
    {
        $this->orderBehaviorStrategyMock = $this
            ->getMockBuilder(OrderBehaviorStrategy::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            DeleteContext::class,
            [
                'orderBehavior' => $this->orderBehaviorStrategyMock
            ]
        );
    }

    /**
     * @return array
     */
    public function orderStatusesAllowedConcreteStrategiesDataProvider()
    {
        return [
            [
                OrderBehaviorStrategy::STATUS_SAVED
            ],
            [
                OrderBehaviorStrategy::STATUS_HOLDED
            ],
            [
                OrderBehaviorStrategy::STATUS_CANCELED
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\DeleteContext::delete()
     * @param $status
     * @dataProvider orderStatusesAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsContextForAllowedConcreteStrategies($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('delete');

        $this->model->delete(1);
    }

    /**
     * @return array
     */
    public function orderStatusesNotAllowedConcreteStrategiesDataProvider()
    {
        return [
            [
                OrderBehaviorStrategy::STATUS_COMPLETED
            ],
            [
                OrderBehaviorStrategy::STATUS_PENDING
            ]
        ];
    }

    /**
     * @covers  \OutsourcingTeam\Customer\Model\Sales\DeleteContext::delete()
     * @param $status
     * @dataProvider orderStatusesNotAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsExceptionIfNotAllowedConcreteStrategy($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('delete');

        $this->expectException(WrongBehaviorException::class);

        $this->model->delete(1);
    }
}
