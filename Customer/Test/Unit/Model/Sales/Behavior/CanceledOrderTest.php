<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Behavior;

use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use OutsourcingTeam\Customer\Model\Sales\Behavior\CanceledOrder;
use OutsourcingTeam\Customer\Model\Sales\Inventory\StockState;
use PHPUnit\Framework\TestCase;

class CanceledOrderTest extends TestCase
{
    /**
     * @var CanceledOrder
     */
    protected $model;
    /**
     * @var ManagerInterface
     */
    protected $messageManagerMock;
    /**
     * @var OrderInterface
     */
    protected $orderMock;
    /**
     * @var StockState
     */
    protected $stockMock;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;

    protected function setUp()
    {
        $this->messageManagerMock = $this
            ->getMockBuilder(ManagerInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $this->stockMock = $this
            ->getMockBuilder(StockState::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderRepositoryMock = $this
            ->getMockBuilder(OrderRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            CanceledOrder::class,
            [
                'messageManager' => $this->messageManagerMock,
                'stock' => $this->stockMock,
                'orderRepository' => $this->orderRepositoryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\CanceledOrder::changeAddress()
     */
    public function testChangeAddress(){
        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->orderRepositoryMock->expects(
            $this->never()
        )->method('save');

        $this->model->changeAddress($this->orderMock, 'some method');
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\CanceledOrder::changeItemQty()
     */
    public function testChangeItemQty(){
        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->orderRepositoryMock->expects(
            $this->never()
        )->method('save');

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, 1);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\CanceledOrder::addItemToOrderBySku()
     */
    public function testAddItemToOrderBySku(){
        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->orderRepositoryMock->expects(
            $this->never()
        )->method('save');

        $this->model->addItemToOrderBySku($this->orderMock, 'some sku');
    }
}
