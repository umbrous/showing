<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Behavior;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Address;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use Magento\Sales\Api\Data\OrderStatusHistoryInterfaceFactory;
use Magento\Sales\Api\OrderAddressRepositoryInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use OutsourcingTeam\Customer\Model\Sales\Address\AddressResolver;
use OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderItemBuilder;
use OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator;
use OutsourcingTeam\Customer\Model\Sales\Inventory\StockState;
use PHPUnit\Framework\TestCase;

class AbstractOrderTest extends TestCase
{
    /**
     * @var AbstractOrder
     */
    protected $model;
    /**
     * @var ProductQtyValidator
     */
    protected $productQtyValidatorMock;
    /**
     * @var ManagerInterface
     */
    protected $messageManagerMock;
    /**
     * @var OrderInterface
     */
    protected $orderMock;
    /**
     * @var StockState
     */
    protected $stockMock;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryMock;
    /**
     * @var Registry
     */
    protected $registryMock;
    /**
     * @var OrderAddressInterface
     */
    protected $orderAddressMock;

    /**
     * @var AddressResolver
     */
    protected $addressResolverMock;
    /**
     * @var Address
     */
    protected $customerAddressModelMock;
    /**
     * @var OrderAddressRepositoryInterface
     */
    protected $orderAddressRepositoryMock;
    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    protected $allmethodsMock;
    /**
     * @var OrderStatusHistoryInterfaceFactory
     */
    protected $orderHistoryInterfaceFactoryMock;
    /**
     * @var OrderStatusHistoryInterface
     */
    protected $orderHistoryInterfaceMock;
    /**
     * @var OrderManagementInterface
     */
    protected $orderManagementInterfaceMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;
    /**
     * @var OrderItemRepositoryInterface
     */
    protected $orderItemRepositoryMock;
    /**
     * @var ProductRepositoryInterface\
     */
    protected $productRepositoryMock;
    /**
     * @var ProductInterface
     */
    protected $productMock;
    /**
     * @var OrderItemBuilder
     */
    protected $orderItemBuilderMock;

    protected function setUp()
    {
        $this->productQtyValidatorMock = $this
            ->getMockBuilder(ProductQtyValidator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->messageManagerMock = $this
            ->getMockBuilder(ManagerInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            //methods exist in realisation but not in interface
            ->setMethods(['getShippingAddress', 'setShippingAddress'])
            ->getMockForAbstractClass();

        $this->stockMock = $this
            ->getMockBuilder(StockState::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderRepositoryMock = $this
            ->getMockBuilder(OrderRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->registryMock = $this
            ->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderAddressMock = $this
            ->getMockBuilder(OrderAddressInterface::class)
            ->setMethods(['setData', 'getData'])
            ->getMockForAbstractClass();

        $this->addressResolverMock = $this
            ->getMockBuilder(AddressResolver::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->customerAddressModelMock = $this
            ->getMockBuilder(Address::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderAddressRepositoryMock = $this
            ->getMockBuilder(OrderAddressRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->allmethodsMock = $this
            ->getMockBuilder(\Magento\Shipping\Model\Config\Source\Allmethods::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderHistoryInterfaceFactoryMock = $this
            ->getMockBuilder(OrderStatusHistoryInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderHistoryInterfaceMock = $this
            ->getMockBuilder(OrderStatusHistoryInterface::class)
            ->getMockForAbstractClass();

        $this->orderManagementInterfaceMock = $this
            ->getMockBuilder(OrderManagementInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemRepositoryMock = $this
            ->getMockBuilder(OrderItemRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->productRepositoryMock = $this
            ->getMockBuilder(ProductRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->productMock = $this
            ->getMockBuilder(ProductInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemBuilderMock = $this
            ->getMockBuilder(OrderItemBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->model = $this->getMockForAbstractClass(
            AbstractOrder::class,
            [
                'messageManager' => $this->messageManagerMock,
                'registry' => $this->registryMock,
                'orderRepository' => $this->orderRepositoryMock,
                'stock' => $this->stockMock,
                'addressResolver' => $this->addressResolverMock,
                'orderAddressRepository' => $this->orderAddressRepositoryMock,
                'allmethods' => $this->allmethodsMock,
                'request' => $this->createMock('\Magento\Framework\App\RequestInterface'),
                'orderStatusHistoryInterfaceFactory' => $this->orderHistoryInterfaceFactoryMock,
                'orderManagement' => $this->orderManagementInterfaceMock,
                'qtyValidator' => $this->productQtyValidatorMock,
                'orderItemRepository' => $this->orderItemRepositoryMock,
                'productRepository' => $this->productRepositoryMock,
                'orderItemBuilder' => $this->orderItemBuilderMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::cancel()
     */
    public function testCancelOrder(){
        $this->orderMock->expects(
            $this->never()
        )->method('setStatus');

        $this->orderMock->expects(
            $this->never()
        )->method('setState');

        $this->orderRepositoryMock->expects(
            $this->never()
        )->method('save');

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');;

        $this->model->cancel($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::delete()
     */
    public function testDeleteOrder(){
        $this->registryMock->expects(
            $this->once()
        )->method('register')
            ->with('isSecureArea',true);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('delete')
            ->with($this->orderMock);

        $this->registryMock->expects(
            $this->once()
        )->method('unregister')
            ->with('isSecureArea');

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->delete($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::changeAddress()
     */
    public function testChangeAddress(){
        $orderShippingAddressEntityId = 1;
        $shippingMethod = 'shipping_method';

        $this->orderMock->expects(
            $this->once()
        )->method('getShippingAddress')
            ->willReturn($this->orderAddressMock);

        $this->orderAddressMock->expects(
            $this->once()
        )->method('getEntityId')
            ->willReturn($orderShippingAddressEntityId);

        $this->addressResolverMock->expects(
            $this->once()
        )->method('getAddress')
            ->willReturn($this->customerAddressModelMock);

        $this->customerAddressModelMock->expects(
            $this->once()
        )->method('getData')
            ->willReturn(['some data']);

        $this->orderAddressMock->expects(
            $this->once()
        )->method('setData')
            ->with(['some data']);

        $this->orderAddressMock->expects(
            $this->once()
        )->method('setEntityId')
            ->with($orderShippingAddressEntityId);

        $this->orderAddressMock->expects(
            $this->once()
        )->method('setAddressType')
            ->with(\Magento\Quote\Model\Quote\Address::ADDRESS_TYPE_SHIPPING);

        $this->orderMock->expects(
            $this->once()
        )->method('setShippingAddress')
            ->with($this->orderAddressMock);

        $this->orderAddressRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderAddressMock);

        $this->allmethodsMock->expects(
            $this->once()
        )->method('toOptionArray')
            ->willReturn([
                0 => [
                    'value' => [
                        0 => [
                            'label' => 'label0',
                            'value' => $shippingMethod
                        ]
                    ]
                ]
            ]);

        $this->orderMock->expects(
            $this->once()
        )->method('setShippingDescription')
            ->with('label0');

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->model->changeAddress($this->orderMock, $shippingMethod);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::setComment()
     */
    public function testSetComment(){
        $comment = 'test comment';
        $status = 'status';

        $this->orderHistoryInterfaceFactoryMock->expects(
            $this->once()
        )->method('create')
            ->willReturn($this->orderHistoryInterfaceMock);

        $this->orderHistoryInterfaceMock->expects(
            $this->once()
        )->method('setComment')
            ->with($comment);

        $this->orderMock->expects(
            $this->once()
        )->method('getStatus')
            ->willReturn($status);

        $this->orderHistoryInterfaceMock->expects(
            $this->once()
        )->method('setStatus')
            ->with($status);

        $this->orderMock->expects(
            $this->once()
        )->method('getEntityId')
            ->willReturn(1);

        $this->orderManagementInterfaceMock->expects(
            $this->once()
        )->method('addComment')
            ->with(1, $this->orderHistoryInterfaceMock);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->model->setComment($this->orderMock, $comment);
    }

    /**
     * @return array
     */
    public function orderItemsAndOrdersDataProvider(){
        return [
            [
                //less items
                12,
                111,
                5000,
                9,
                4109
            ],
            [
                //more items
                113,
                111,
                5000,
                9,
                5018
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::changeItemQty()
     * @dataProvider orderItemsAndOrdersDataProvider
     * @param $newQty int
     * @param $orderedQty int
     * @param $currentOrderSubtotal int
     * @param $itemPrice int
     * @param $expectedPrice int
     */
    public function testChangeItemQty($newQty, $orderedQty, $currentOrderSubtotal, $itemPrice, $expectedPrice){
        $this->orderItemMock->expects(
            $this->once()
        )->method('getQtyOrdered')
            ->willReturn($orderedQty);

        $this->orderItemMock->expects(
            $this->once()
        )->method('setQtyOrdered')
            ->with($newQty);

        $this->orderItemRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderItemMock);

        $this->orderMock->expects(
            $this->once()
        )->method('getSubtotal')
            ->willReturn($currentOrderSubtotal);

        $this->orderItemMock->expects(
            $this->once()
        )->method('getPrice')
            ->willReturn($itemPrice);

        //Change order subtotal after change item qty
        $this->orderMock->expects(
            $this->once()
        )->method('setSubtotal')
            ->with($expectedPrice);

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, $newQty);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::removeItemFromOrder()
     */
    public function testRemoveItemFromOrderIfInOrderMoreThenOneItem(){
        $orderSubtotal = 1000;
        $itemToDeletePrice = 10;
        $itemToDeleteQty = 13;
        $expectedNewSubtotal = 870;

        //item to stay
        $orderItemMock1 = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        //item to delete
        $orderItemMock2 = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock->expects(
            $this->once()
        )->method('getItems')
            ->willReturn([
                $orderItemMock1,
                $orderItemMock2
            ]);

        $orderItemMock1->expects(
            $this->once()
        )->method('getItemId')
            ->willReturn(1);

        $orderItemMock2->expects(
            $this->once()
        )->method('getItemId')
            ->willReturn(2);

        $this->orderItemMock->expects(
            $this->exactly(2)
        )->method('getItemId')
            ->willReturn(2);

        $this->orderItemRepositoryMock->expects(
            $this->once()
        )->method('delete')
            ->with($orderItemMock2);

        $this->orderMock->expects(
            $this->once()
        )->method('getSubtotal')
            ->willReturn($orderSubtotal);

        $orderItemMock2->expects(
            $this->once()
        )->method('getPrice')
            ->willReturn($itemToDeletePrice);

        $orderItemMock2->expects(
            $this->once()
        )->method('getQtyOrdered')
            ->willReturn($itemToDeleteQty);

        $this->orderMock->expects(
            $this->once()
        )->method('setSubtotal')
            ->with($expectedNewSubtotal);

        $this->orderMock->expects(
            $this->once()
        )->method('setItems')
            ->with([
                $orderItemMock1
            ]);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->model->removeItemFromOrder($this->orderMock, $this->orderItemMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::removeItemFromOrder()
     */
    public function testRemoveItemFromOrderIfInOrderOneItemWillRemoveOrderComplitelly(){
        $orderItemMock1 = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock->expects(
            $this->once()
        )->method('getItems')
            ->willReturn([
                $orderItemMock1
            ]);

        $this->registryMock->expects(
            $this->once()
        )->method('register')
            ->with('isSecureArea',true);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('delete')
            ->with($this->orderMock);

        $this->registryMock->expects(
            $this->once()
        )->method('unregister')
            ->with('isSecureArea');

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->removeItemFromOrder($this->orderMock, $this->orderItemMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\AbstractOrder::addItemToOrderBySku()
     */
    public function testAddItemBySku(){
        $sku = 'someSku';
        $qty = 10;
        $orderSubtotal = 1000;
        $itemTotal = 100;
        $expectedNewOrderSubtotal = 1100;

        //item to stay
        $orderItemMock1 = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        //item to delete
        $orderItemMock2 = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->productRepositoryMock->expects(
            $this->once()
        )->method('get')
            ->with($sku)
            ->willReturn($this->productMock);

        $this->orderMock->expects(
            $this->exactly(2)
        )->method('getItems')
            ->willReturn([
                $orderItemMock1,
                $orderItemMock2
            ]);

        $this->productMock->expects(
            $this->once()
        )->method('getId')
            ->willReturn(1);

        $this->stockMock->expects(
            $this->once()
        )->method('checkQty')
            ->willReturn(1, $qty);

        $this->orderItemBuilderMock->expects(
            $this->once()
        )->method('create')
            ->with($this->productMock, $qty)
            ->willReturn($this->orderItemMock);

        $this->orderMock->expects(
            $this->once()
        )->method('getSubtotal')
            ->willReturn($orderSubtotal);

        $this->orderItemMock->expects(
            $this->once()
        )->method('getRowTotal')
            ->willReturn($itemTotal);

        $this->orderMock->expects(
            $this->once()
        )->method('setSubtotal')
            ->with($expectedNewOrderSubtotal);

        $this->orderMock->expects(
            $this->once()
        )->method('getEntityId')
            ->willReturn(1);

        $this->orderItemMock->expects(
            $this->once()
        )->method('setOrderId')
            ->with(1);

        $this->orderMock->expects(
            $this->once()
        )->method('setItems')
            ->with([
                $orderItemMock1,
                $orderItemMock2,
                $this->orderItemMock
            ]);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->model->addItemToOrderBySku($this->orderMock, $sku, $qty);
    }
}
