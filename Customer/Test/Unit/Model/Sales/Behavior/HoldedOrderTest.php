<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Behavior;

use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\OutOfStockException;
use OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator;
use OutsourcingTeam\Customer\Model\Sales\Inventory\StockState;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class HoldedOrderTest extends TestCase
{
    /**
     * @var HoldedOrder
     */
    protected $model;
    /**
     * @var ProductQtyValidator
     */
    protected $productQtyValidatorMock;
    /**
     * @var ManagerInterface
     */
    protected $messageManagerMock;
    /**
     * @var OrderInterface
     */
    protected $orderMock;
    /**
     * @var StockState
     */
    protected $stockMock;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;

    protected function setUp()
    {
        $this->productQtyValidatorMock = $this
            ->getMockBuilder(ProductQtyValidator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->messageManagerMock = $this
            ->getMockBuilder(ManagerInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            //methods exist in realisation but not in interface
            ->setMethods(['getShippingAddress', 'setShippingAddress'])
            ->getMockForAbstractClass();

        $this->stockMock = $this
            ->getMockBuilder(StockState::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderRepositoryMock = $this
            ->getMockBuilder(OrderRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            HoldedOrder::class,
            [
                'messageManager' => $this->messageManagerMock,
                'qtyValidator' => $this->productQtyValidatorMock,
                'stock' => $this->stockMock,
                'orderRepository' => $this->orderRepositoryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder::process()
     */
    public function testProcessOrder(){
        $this->orderMock->expects(
            $this->once()
        )->method('setStatus')
            ->with(OrderBehaviorStrategy::STATUS_PENDING);

        $this->orderMock->expects(
            $this->once()
        )->method('setState')
            ->with(Order::STATE_NEW);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->process($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder::cancel()
     */
    public function testCancelOrder(){
        $this->orderMock->expects(
            $this->once()
        )->method('setStatus')
            ->with(OrderBehaviorStrategy::STATUS_CANCELED);

        $this->orderMock->expects(
            $this->once()
        )->method('setState')
            ->with(Order::STATE_CANCELED);

        $this->stockMock->expects(
            $this->once()
        )->method('addItemsToStockByOrder')
            ->with($this->orderMock);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->cancel($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder::changeItemQty()
     */
    public function testChangeItemQtyIfNewQtySmaller(){
        $newQty = 12;
        $oldQty = 13;
        $differenceBetweenQtys = -1;

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getQtyOrdered')
            ->willReturn($oldQty);

        $this->productQtyValidatorMock->expects(
            $this->never()
        )->method('checkQtyByItem');

        $this->stockMock->expects(
            $this->once()
        )->method('changeStockItemOnChangeOrderItemQty')
            ->with($this->orderItemMock, $differenceBetweenQtys);

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, $newQty);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder::changeItemQty()
     */
    public function testChangeItemQtyIfNewQtySBiggerAndInStock(){
        $newQty = 12;
        $oldQty = 11;
        $diff = 1;

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getQtyOrdered')
            ->willReturn($oldQty);

        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkQtyByItem')
            ->with($this->orderItemMock, $diff)
            //in stock
            ->willReturn(true);

        $this->stockMock->expects(
            $this->once()
        )->method('changeStockItemOnChangeOrderItemQty')
            ->with($this->orderItemMock, $diff);

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, $newQty);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder::changeItemQty()
     */
    public function testChangeItemQtyIfNewQtySBiggerAndOutOfStock(){
        $newQty = 12;
        $oldQty = 11;
        $diff = 1;

        $this->orderItemMock->expects(
            $this->atLeastOnce()
        )->method('getQtyOrdered')
            ->willReturn($oldQty);

        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkQtyByItem')
            ->with($this->orderItemMock, $diff)
            //out of stock
            ->willReturn(false);

        $this->stockMock->expects(
            $this->never()
        )->method('changeStockItemOnChangeOrderItemQty');

        $this->expectException(OutOfStockException::class);

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, $newQty);
    }
}
