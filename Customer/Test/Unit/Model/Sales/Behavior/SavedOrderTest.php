<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Behavior;

use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use OutsourcingTeam\Customer\Model\Sales\Behavior\OutOfStockException;
use OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder;
use OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator;
use OutsourcingTeam\Customer\Model\Sales\Inventory\StockState;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class SavedOrderTest extends TestCase
{
    /**
     * @var SavedOrder
     */
    protected $model;
    /**
     * @var ProductQtyValidator
     */
    protected $productQtyValidatorMock;
    /**
     * @var ManagerInterface
     */
    protected $messageManagerMock;
    /**
     * @var OrderInterface
     */
    protected $orderMock;
    /**
     * @var StockState
     */
    protected $stockMock;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;

    protected function setUp()
    {
        $this->productQtyValidatorMock = $this
            ->getMockBuilder(ProductQtyValidator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->messageManagerMock = $this
            ->getMockBuilder(ManagerInterface::class)
            ->getMockForAbstractClass();

        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            //methods exist in realisation but not in interface
            ->setMethods(['getShippingAddress', 'setShippingAddress'])
            ->getMockForAbstractClass();

        $this->stockMock = $this
            ->getMockBuilder(StockState::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderRepositoryMock = $this
            ->getMockBuilder(OrderRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            SavedOrder::class,
            [
                'messageManager' => $this->messageManagerMock,
                'qtyValidator' => $this->productQtyValidatorMock,
                'stock' => $this->stockMock,
                'orderRepository' => $this->orderRepositoryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder::process()
     */
    public function testProcessOrderIfItemsInStock(){
        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkOrderQtyBeforeStockChange')
            //in stock
            ->willReturn(true);

        $this->orderMock->expects(
            $this->once()
        )->method('setStatus')
            ->with(OrderBehaviorStrategy::STATUS_PENDING);

        $this->orderMock->expects(
            $this->once()
        )->method('setState')
            ->with(Order::STATE_NEW);

        $this->stockMock->expects(
            $this->once()
        )->method('subtractItemsFromStockByOrder')
            ->with($this->orderMock);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->process($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder::process()
     */
    public function testProcessOrderIfItemsOutOfStock(){
        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkOrderQtyBeforeStockChange')
            //out of stock
            ->willReturn(false);

        $this->orderMock->expects(
            $this->never()
        )->method('setStatus');

        $this->orderMock->expects(
            $this->never()
        )->method('setState');

        $this->stockMock->expects(
            $this->never()
        )->method('subtractItemsFromStockByOrder');

        $this->orderRepositoryMock->expects(
            $this->never()
        )->method('save');

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->process($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder::hold()
     */
    public function testHoldOrderIfItemsInStock(){
        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkOrderQtyBeforeStockChange')
            //in stock
            ->willReturn(true);

        $this->orderMock->expects(
            $this->once()
        )->method('setStatus')
            ->with(OrderBehaviorStrategy::STATUS_HOLDED);

        $this->orderMock->expects(
            $this->once()
        )->method('setState')
            ->with(Order::STATE_HOLDED);

        $this->stockMock->expects(
            $this->once()
        )->method('subtractItemsFromStockByOrder')
            ->with($this->orderMock);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->hold($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder::hold()
     */
    public function testHoldOrderIfItemsOutOfStock(){
        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkOrderQtyBeforeStockChange')
            //out of stock
            ->willReturn(false);

        $this->orderMock->expects(
            $this->never()
        )->method('setStatus');

        $this->orderMock->expects(
            $this->never()
        )->method('setState');

        $this->stockMock->expects(
            $this->never()
        )->method('subtractItemsFromStockByOrder');

        $this->orderRepositoryMock->expects(
            $this->never()
        )->method('save');

        $this->messageManagerMock->expects(
            $this->once()
        )->method('addMessage');

        $this->model->hold($this->orderMock);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder::changeItemQty()
     */
    public function testChangeItemQtyIfInStock(){
        $newQty = 12;

        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkQtyByItem')
            ->with($this->orderItemMock, $newQty)
            //in stock
            ->willReturn(true);

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, $newQty);
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder::changeItemQty()
     */
    public function testChangeItemQtyIfOutOfStock(){
        $newQty = 12;

        $this->productQtyValidatorMock->expects(
            $this->once()
        )->method('checkQtyByItem')
            ->with($this->orderItemMock, $newQty)
            //out of stock
            ->willReturn(false);

        $this->expectException(OutOfStockException::class);

        $this->model->changeItemQty($this->orderMock, $this->orderItemMock, $newQty);
    }
}
