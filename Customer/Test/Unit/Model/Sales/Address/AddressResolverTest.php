<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Address;

use Magento\Customer\Model\Address;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use OutsourcingTeam\Checkout\Model\Checkout\ShippingType;
use OutsourcingTeam\Customer\Model\Sales\Address\AddressResolver;
use OutsourcingTeam\Customer\Model\Sales\Address\ShippingCompanyAddressDecorator;
use PHPUnit\Framework\TestCase;

class AddressResolverTest extends TestCase
{
    /**
     * @var AddressResolver
     */
    protected $model;
    /**
     * @var RequestInterface
     */
    protected $requestMock;
    /**
     * @var Session
     */
    protected $customerSessionMock;
    /**
     * @var Customer
     */
    protected $customerMock;
    /**
     * @var ShippingCompanyAddressDecorator
     */
    protected $shippingCompanyAddressMock;
    /**
     * @var Address
     */
    protected $addressMock;

    protected function setUp()
    {
        $this->requestMock = $this
            ->getMockBuilder(RequestInterface::class)
            ->getMockForAbstractClass();

        $this->customerSessionMock = $this
            ->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->customerMock = $this
            ->getMockBuilder(Customer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->shippingCompanyAddressMock = $this
            ->getMockBuilder(ShippingCompanyAddressDecorator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->addressMock = $this
            ->getMockBuilder(Address::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            AddressResolver::class,
            [
                'request' => $this->requestMock,
                'customerSession' => $this->customerSessionMock,
                'shippingCompanyAddress' => $this->shippingCompanyAddressMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Address\AddressResolver::getAddress()
     */
    public function testGetShippingCompanyAddress(){
        $shippingType = ShippingType::SHIPPING_COMPANY;

        $this->requestMock->expects(
            $this->once()
        )->method('getParam')
            ->with('shippingType')
            ->willReturn($shippingType);

        $this->customerSessionMock->expects(
            $this->once()
        )->method('getCustomer')
            ->willReturn($this->customerMock);

        $this->shippingCompanyAddressMock->expects(
            $this->once()
        )->method('setDataByCustomerRequest')
            ->with($this->customerMock)
            ->willReturn($this->addressMock);

        $this->assertEquals($this->addressMock, $this->model->getAddress());
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Address\AddressResolver::getAddress()
     */
    public function testGetShippingAddress(){
        $shippingType = ShippingType::PLAIN_SHIPPING;
        $shippingAddressId = 1;

        $this->requestMock->expects(
            $this->at(0)
        )->method('getParam')
            ->with('shippingType')
            ->willReturn($shippingType);

        $this->customerSessionMock->expects(
            $this->once()
        )->method('getCustomer')
            ->willReturn($this->customerMock);

        $this->requestMock->expects(
            $this->at(1)
        )->method('getParam')
            ->with('shippingAddress')
            ->willReturn($shippingAddressId);

        $this->customerMock->expects(
            $this->once()
        )->method('getAddressById')
            ->with($shippingAddressId)
            ->willReturn($this->addressMock);

        $this->assertEquals($this->addressMock, $this->model->getAddress());
    }
}
