<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Data;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTotalRecounter;
use PHPUnit\Framework\TestCase;

class OrderTotalRecounterTest extends TestCase
{
    /**
     * @var OrderTotalRecounter
     */
    protected $model;
    /**
     * @var OrderInterface
     */
    protected $orderMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepositoryMock;
    /**
     * @var ProductInterface
     */
    protected $productMock;
    /**
     * @var OrderItemRepositoryInterface
     */
    protected $orderItemRepositoryMock;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryMock;

    protected function setUp()
    {
        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->productRepositoryMock = $this
            ->getMockBuilder(ProductRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->productMock = $this
            ->getMockBuilder(ProductInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemRepositoryMock = $this
            ->getMockBuilder(OrderItemRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->orderRepositoryMock = $this
            ->getMockBuilder(OrderRepositoryInterface::class)
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            OrderTotalRecounter::class,
            [
                'productRepository' => $this->productRepositoryMock,
                'orderItemRepository' => $this->orderItemRepositoryMock,
                'orderRepository' => $this->orderRepositoryMock
            ]
        );
    }

    public function testRecountOrderTotal()
    {
        $arrayOfItems = [
            $this->orderItemMock,
            $this->orderItemMock
        ];

        $productId = 1;
        $productPrice  = 100;
        $orderItemQty = 3;
        $orderItemRowTotal = 300;
        $orderTotal = 600;

        $this->orderMock->expects(
            $this->once()
        )->method('getItems')
            ->willReturn($arrayOfItems);

        $this->orderItemMock->expects(
            $this->exactly(2)
        )->method('getProductId')
            ->willReturn($productId);

        $this->productRepositoryMock->expects(
            $this->exactly(2)
        )->method('getById')
            ->with($productId)
            ->willReturn($this->productMock);

        $this->productMock->expects(
            $this->exactly(2)
        )->method('getPrice')
            ->willReturn($productPrice);

        $this->orderItemMock->expects(
            $this->exactly(2)
        )->method('getQtyOrdered')
            ->willReturn($orderItemQty);

        $this->orderItemMock->expects(
            $this->exactly(2)
        )->method('setPrice')
            ->with($productPrice);

        $this->orderItemMock->expects(
            $this->exactly(2)
        )->method('setRowTotal')
            ->with($orderItemRowTotal);

        $this->orderItemRepositoryMock->expects(
            $this->exactly(2)
        )->method('save')
            ->with($this->orderItemMock);

        $this->orderMock->expects(
            $this->once()
        )->method('setSubtotal')
            ->with($orderTotal);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('save')
            ->with($this->orderMock);

        $this->model->recount($this->orderMock);
    }
}
