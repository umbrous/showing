<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Data;

use Magento\Checkout\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTable;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTableItem;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTableItemBuilder;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTotalRecounter;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class OrderTableTest extends TestCase
{
    /**
     * @var OrderTable
     */
    protected $model;
    /**
     * @var OrderInterface
     */
    protected $orderMock;
    /**
     * @var Session
     */
    protected $customerSessionMock;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;
    /**
     * @var OrderTableItemBuilder
     */
    protected $itemsBuilderMock;
    /**
     * @var OrderTableItem
     */
    protected $tableItemMock;
    /**
     * @var Data
     */
    protected $checkoutHelper;
    /**
     * @var OrderTotalRecounter
     */
    protected $orderTotalRecounterMock;


    protected function setUp()
    {
        $this->orderMock = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $this->customerSessionMock = $this
            ->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->itemsBuilderMock = $this
            ->getMockBuilder(OrderTableItemBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->tableItemMock = $this
            ->getMockBuilder(OrderTableItem::class)
            ->disableOriginalConstructor()
            ->setMethods(['toArray'])
            ->getMock();

        $this->orderRepositoryMock = $this
            ->getMockBuilder(OrderRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $this->checkoutHelper = $this
            ->getMockBuilder(Data::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderTotalRecounterMock = $this
            ->getMockBuilder(OrderTotalRecounter::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            OrderTable::class,
            [
                'customerSession' => $this->customerSessionMock,
                'orderRepository' => $this->orderRepositoryMock,
                'itemBuilder' => $this->itemsBuilderMock,
                'checkoutHelper' => $this->checkoutHelper,
                'totalRecounter' => $this->orderTotalRecounterMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Data\OrderTable::getItemsData()
     */
    public function testGetItemsData(){
        $orderId = 1;
        $customerId = 2;
        $orderSubtotal = 100;
        $itemArray = [];
        $formattedPriceString = '** 100 **';

        $orderStatus = OrderBehaviorStrategy::STATUS_HOLDED;

        $expectedResult = [
          'items' => [[]],
          'subtotal' => $formattedPriceString
        ];

        $this->customerSessionMock->expects(
            $this->once()
        )->method('getCustomerId')
            ->willReturn($customerId);

        $this->orderRepositoryMock->expects(
            $this->once()
        )->method('get')
            ->with($orderId)
            ->willReturn($this->orderMock);

        $this->orderMock->expects(
            $this->once()
        )->method('getCustomerId')
            ->willReturn($customerId);

        $this->orderMock->expects(
            $this->once()
        )->method('getStatus')
            ->willReturn($orderStatus);

        $this->orderTotalRecounterMock->expects(
            $this->once()
        )->method('recount')
            ->with($this->orderMock);

        $this->orderMock->expects(
            $this->once()
        )->method('getItems')
            ->willReturn([$this->orderItemMock]);

        $this->itemsBuilderMock->expects(
            $this->once()
        )->method('create')
            ->with($this->orderItemMock)
            ->willReturn($this->tableItemMock);

        $this->tableItemMock->expects(
            $this->once()
        )->method('toArray')
            ->willReturn($itemArray);

        $this->orderMock->expects(
            $this->once()
        )->method('getSubtotal')
            ->willReturn($orderSubtotal);

        $this->checkoutHelper->expects(
            $this->once()
        )->method('formatPrice')
            ->with($orderSubtotal)
            ->willReturn($formattedPriceString);

        $this->assertEquals($expectedResult, $this->model->getItemsData($orderId));
    }
}
