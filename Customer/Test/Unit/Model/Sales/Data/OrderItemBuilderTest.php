<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Data;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderItemInterfaceFactory;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderItemBuilder;
use PHPUnit\Framework\TestCase;

class OrderItemBuilderTest extends TestCase
{
    /**
     * @var OrderItemBuilder
     */
    protected $model;
    /**
     * @var OrderItemInterfaceFactory
     */
    protected $orderItemInterfaceFactoryMock;
    /**
     * @var ProductInterface
     */
    protected $productMock;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;

    protected function setUp()
    {
        $this->orderItemInterfaceFactoryMock = $this
            ->getMockBuilder(OrderItemInterfaceFactory::class)
            ->setMethods(['create'])
            ->getMockForAbstractClass();

        $this->productMock = $this
            ->getMockBuilder(ProductInterface::class)
            ->getMockForAbstractClass();

        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            OrderItemBuilder::class,
            [
                'orderItemInterfaceFactory' => $this->orderItemInterfaceFactoryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Data\OrderItemBuilder::create()
     */
    public function testCreate(){
        $qty = 15;
        $sku = 'some sku';
        $productId = 1;
        $price = 10;
        $itemTotalPrice = 150;

        $this->orderItemInterfaceFactoryMock->expects(
            $this->once()
        )->method('create')
        ->willReturn($this->orderItemMock);

        //create sku
        $this->productMock->expects(
            $this->once()
        )->method('getSku')
            ->willReturn($sku);
        $this->orderItemMock->expects(
            $this->once()
        )->method('setSku')
            ->with($sku);

        //create product id
        $this->productMock->expects(
            $this->once()
        )->method('getId')
            ->willReturn($productId);
        $this->orderItemMock->expects(
            $this->once()
        )->method('setProductId')
            ->with($productId);

        //set Qty
        $this->orderItemMock->expects(
            $this->once()
        )->method('setQtyOrdered')
            ->with($qty);

        //create price
        $this->productMock->expects(
            $this->atLeastOnce()
        )->method('getPrice')
            ->willReturn($price);
        $this->orderItemMock->expects(
            $this->once()
        )->method('setPrice')
            ->with($price);

        //total price
        $this->orderItemMock->expects(
            $this->once()
        )->method('setRowTotal')
            ->with($itemTotalPrice);

        //create product type
        $this->productMock->expects(
            $this->atLeastOnce()
        )->method('getTypeId')
            ->willReturn(1);
        $this->orderItemMock->expects(
            $this->once()
        )->method('setProductType')
            ->with(1);

        $this->assertInstanceOf(OrderItemInterface::class, $this->model->create($this->productMock, $qty));
    }
}
