<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales\Data;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTableItem;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTableItemBuilder;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTableItemFactory;
use PHPUnit\Framework\TestCase;

class OrderTableItemBuilderTest extends TestCase
{
    /**
     * @var OrderTableItemBuilder
     */
    protected $model;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemMock;
    /**
     * @var OrderTableItemFactory
     */
    protected $orderTableItemFactoryMock;
    /**
     * @var OrderTableItem
     */
    protected $orderTableItemMock;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepositoryMock;
    /**
     * @var ProductInterface
     */
    protected $productMock;

    protected function setUp()
    {
        $this->orderItemMock = $this
            ->getMockBuilder(OrderItemInterface::class)
            ->setMethods(['getData'])
            ->getMockForAbstractClass();

        $this->orderTableItemFactoryMock = $this
            ->getMockBuilder(OrderTableItemFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderTableItemMock = $this
            ->getMockBuilder(OrderTableItem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->productRepositoryMock = $this
            ->getMockBuilder(ProductRepositoryInterface::class)
            ->getMockForAbstractClass();

        $this->productMock = $this
            ->getMockBuilder(ProductInterface::class)
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            OrderTableItemBuilder::class,
            [
                'orderItemFactory' => $this->orderTableItemFactoryMock,
                'productRepository' => $this->productRepositoryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\Data\OrderTableItemBuilder::create()
     */
    public function testBuildFromOrderItem(){
        $this->orderTableItemFactoryMock->expects(
            $this->once()
        )->method('create')
            ->willReturn($this->orderTableItemMock);

        $this->productRepositoryMock->expects(
            $this->once()
        )->method('getById')
            ->willReturn($this->productMock);

        $this->assertInstanceOf(OrderTableItem::class, $this->model->create($this->orderItemMock));
    }
}
