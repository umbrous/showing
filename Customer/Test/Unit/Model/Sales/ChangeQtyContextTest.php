<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;
use OutsourcingTeam\Customer\Model\Sales\ChangeQtyContext;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class ChangeQtyContextTest extends TestCase
{
    /**
     * @var ChangeQtyContext
     */
    protected $model;
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehaviorStrategyMock;

    protected function setUp()
    {
        $this->orderBehaviorStrategyMock = $this
            ->getMockBuilder(OrderBehaviorStrategy::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            ChangeQtyContext::class,
            [
                'orderBehavior' => $this->orderBehaviorStrategyMock
            ]
        );
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesAllowedConcreteStrategiesDataProvider(){
        return [
            [
                OrderBehaviorStrategy::STATUS_SAVED
            ],
            [
                OrderBehaviorStrategy::STATUS_HOLDED
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\ChangeQtyContext::changeQty()
     * @param $status
     * @dataProvider orderIdsAndStatusesAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsContextForAllowedConcreteStrategies($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('changeQty')->with(3, 2);

        $this->model->changeQty(1, 2, 3);
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider(){
        return [
            [
                OrderBehaviorStrategy::STATUS_CANCELED
            ],
            [
                OrderBehaviorStrategy::STATUS_COMPLETED
            ],
            [
                OrderBehaviorStrategy::STATUS_PENDING
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\ChangeQtyContext::changeQty()
     * @param $status
     * @dataProvider orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsExceptionIfNotAllowedConcreteStrategy($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('changeQty');

        $this->expectException(WrongBehaviorException::class);

        $this->model->changeQty(1, 2, 3);
    }
}
