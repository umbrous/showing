<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\AddBySkuContext;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use PHPUnit\Framework\TestCase;

class AddBySkuContextTest extends TestCase
{
    /**
     * @var AddBySkuContext
     */
    protected $model;
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehaviorStrategyMock;

    protected function setUp()
    {
        $this->orderBehaviorStrategyMock = $this
            ->getMockBuilder(OrderBehaviorStrategy::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            AddBySkuContext::class,
            [
                'orderBehavior' => $this->orderBehaviorStrategyMock
            ]
        );
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesAllowedConcreteStrategiesDataProvider(){
        return [
            [
                OrderBehaviorStrategy::STATUS_SAVED
            ],
            [
                OrderBehaviorStrategy::STATUS_HOLDED
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\AddBySkuContext::addBySku()
     * @param $status
     * @dataProvider orderIdsAndStatusesAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsContextForAllowedConcreteStrategies($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('addBySku')->with('Some sku', 3);

        $this->model->addBySku(1, 'Some sku', 3);
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider(){
        return [
            [
                OrderBehaviorStrategy::STATUS_CANCELED
            ],
            [
                OrderBehaviorStrategy::STATUS_COMPLETED
            ],
            [
                OrderBehaviorStrategy::STATUS_PENDING
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\AddBySkuContext::addBySku()
     * @param $status
     * @dataProvider orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsExceptionIfNotAllowedConcreteStrategy($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('addBySku');

        $this->expectException(WrongBehaviorException::class);

        $this->model->addBySku(1, 'Some sku', 3);
    }
}
