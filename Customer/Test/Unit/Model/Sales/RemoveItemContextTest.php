<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use OutsourcingTeam\Customer\Model\Sales\RemoveItemContext;
use PHPUnit\Framework\TestCase;

class RemoveItemContextTest extends TestCase
{
    /**
     * @var RemoveItemContext
     */
    protected $model;
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehaviorStrategyMock;

    protected function setUp()
    {
        $this->orderBehaviorStrategyMock = $this
            ->getMockBuilder(OrderBehaviorStrategy::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            RemoveItemContext::class,
            [
                'orderBehavior' => $this->orderBehaviorStrategyMock
            ]
        );
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesAllowedConcreteStrategiesDataProvider(){
        return [
            [
                OrderBehaviorStrategy::STATUS_SAVED
            ],
            [
                OrderBehaviorStrategy::STATUS_HOLDED
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\RemoveItemContext::removeItem()
     * @param $status
     * @dataProvider orderIdsAndStatusesAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsContextForAllowedConcreteStrategies($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('removeItem')->with(1);

        $this->model->removeItem(1, 1);
    }

    /**
     * @return array
     */
    public function orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider(){
        return [
            [
                OrderBehaviorStrategy::STATUS_CANCELED
            ],
            [
                OrderBehaviorStrategy::STATUS_COMPLETED
            ],
            [
                OrderBehaviorStrategy::STATUS_PENDING
            ]
        ];
    }

    /**
     * @covers \OutsourcingTeam\Customer\Model\Sales\RemoveItemContext::removeItem()
     * @param $status
     * @dataProvider orderIdsAndStatusesNotAllowedConcreteStrategiesDataProvider
     */
    public function testRemoveItemsExceptionIfNotAllowedConcreteStrategy($status)
    {
        $this->orderBehaviorStrategyMock->expects(
            $this->once()
        )->method('initOrder')
            ->willReturn($status);

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('setBehavior');

        $this->orderBehaviorStrategyMock->expects(
            $this->never()
        )->method('removeItem');

        $this->expectException(WrongBehaviorException::class);

        $this->model->removeItem(1, 1);
    }
}