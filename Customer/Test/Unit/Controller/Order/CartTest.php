<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Controller\Order;

use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use OutsourcingTeam\Customer\Controller\Order\Cart;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTable;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{
    /**
     * @var Cart
     */
    protected $controller;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactoryMock;
    /**
     * @var Redirect
     */
    protected $resultRedirectMock;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $requestMock;
    /**
     * @var OrderTable
     */
    protected $orderTableMock;
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactoryMock;
    protected $resultJsonMock;

    protected function setUp()
    {
        $this->resultRedirectFactoryMock = $this
            ->getMockBuilder(RedirectFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultRedirectMock = $this
            ->getMockBuilder(Redirect::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultJsonFactoryMock = $this
            ->getMockBuilder(JsonFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultJsonMock = $this
            ->getMockBuilder(Json::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->requestMock = $this
            ->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->getMockForAbstractClass();

        $this->orderTableMock = $this
            ->getMockBuilder(OrderTable::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->controller = $objectManager->getObject(
            Cart::class,
            [
                'resultRedirectFactory' => $this->resultRedirectFactoryMock,
                '_request' => $this->requestMock,
                'order' => $this->orderTableMock,
                'resultJsonFactory' => $this->resultJsonFactoryMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Controller\Order\Cart::execute()
     */
    public function testExecuteWithoutParamOrderId(){
        $this->requestMock->expects(
            $this->once()
        )->method('getParam')
            ->with('order_id')
            ->willReturn(null);

        $this->resultRedirectFactoryMock->expects(
            $this->once()
        )->method('create')
            ->willReturn($this->resultRedirectMock);

        $this->resultRedirectMock->expects(
            $this->once()
        )->method('setPath')
            ->with('cms/index/index')
            ->willReturn($this->resultRedirectMock);

        $this->assertSame($this->resultRedirectMock, $this->controller->execute());
    }

    /**
     * @covers \OutsourcingTeam\Customer\Controller\Order\Cart::execute()
     */
    public function testExecuteWithExistingOrderId(){
        $existingOrderId = 1;
        $itemsArray = [1, 2 ,5, '123'];

        $this->requestMock->expects(
            $this->once()
        )->method('getParam')
            ->with('order_id')
            ->willReturn($existingOrderId);

        $this->orderTableMock->expects(
            $this->once()
        )->method('getItemsData')
            ->with($existingOrderId)
            ->willReturn($itemsArray);

        $this->resultJsonFactoryMock->expects(
            $this->once()
        )->method('create')
            ->willReturn($this->resultJsonMock);

        $this->resultJsonMock->expects(
            $this->once()
        )->method('setData')
            ->with($itemsArray)
            ->willReturn($this->resultJsonMock);

        $this->assertSame($this->resultJsonMock, $this->controller->execute());
    }

    /**
     * @covers \OutsourcingTeam\Customer\Controller\Order\Cart::execute()
     */
    public function testExecuteWithNOTExistingOrderId(){
        $notExistingOrderId = 15;

        $this->requestMock->expects(
            $this->once()
        )->method('getParam')
            ->with('order_id')
            ->willReturn($notExistingOrderId);

        $this->orderTableMock->expects(
            $this->once()
        )->method('getItemsData')
            ->with($notExistingOrderId)
            ->will($this->throwException(new NoSuchEntityException()));

        $this->resultJsonFactoryMock->expects(
            $this->once()
        )->method('create')
            ->willReturn($this->resultJsonMock);

        $this->resultJsonMock->expects(
            $this->once()
        )->method('setData')
            ->with(false)
            ->willReturn($this->resultJsonMock);

        $this->assertSame($this->resultJsonMock, $this->controller->execute());
    }
}
