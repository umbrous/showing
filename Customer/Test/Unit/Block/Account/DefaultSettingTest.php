<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Test\Unit\Block\Account;

use OutsourcingTeam\Customer\Block\Account\DefaultSetting;
use PHPUnit\Framework\TestCase;

class DefaultSettingTest extends TestCase
{
    /**
     * @var DefaultSetting
     */
    protected $block;
    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    protected $allMethodsMock;

    protected function setUp()
    {
        $this->allMethodsMock = $this
            ->getMockBuilder(\Magento\Shipping\Model\Config\Source\Allmethods::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->block = $objectManager->getObject(
            DefaultSetting::class,
            [
                'allmethods' => $this->allMethodsMock
            ]
        );
    }

    /**
     * @covers \OutsourcingTeam\Customer\Block\Account\DefaultSetting::getAllMethods()
     */
    public function testGetAllMethodsWithoutFreeshipping(){
        $allMethodsArray = [
            'someKey' => '',
            'freeshipping' => '',
            'someOtherKey' => ''
        ];
        $expectedAllMethodsArray = [
            'someKey' => '',
            'someOtherKey' => ''
        ];

        $this->allMethodsMock->expects(
            $this->once()
        )->method('toOptionArray')
            ->with(true)
            ->willReturn($allMethodsArray);

      $this->assertEquals($expectedAllMethodsArray, $this->block->getAllMethods());
    }
}
