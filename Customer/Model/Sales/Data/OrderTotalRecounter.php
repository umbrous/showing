<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */
declare(strict_types=1);

namespace OutsourcingTeam\Customer\Model\Sales\Data;

use Magento\Catalog\Api\Data\ProductRenderInterface;
use Magento\Catalog\Api\ProductRenderListInterface;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;

class OrderTotalRecounter
{
    /**
     * @var OrderItemRepositoryInterface
     */
    protected $orderItemRepository;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var ProductRenderListInterface
     */
    private $productRenderList;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * OrderTotalRecounter constructor.
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param ProductRenderListInterface $productRenderList
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        OrderItemRepositoryInterface $orderItemRepository,
        OrderRepositoryInterface $orderRepository,
        ProductRenderListInterface $productRenderList,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        StoreManagerInterface $storeManager
    ) {
        $this->orderItemRepository = $orderItemRepository;
        $this->orderRepository = $orderRepository;
        $this->productRenderList = $productRenderList;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->storeManager = $storeManager;
    }

    /**
     * @param OrderInterface $order
     */
    public function recount(OrderInterface $order)
    {
        $orderSubtotal = 0;
        $orderSubtotalIncludingTax = 0;
        $items = $order->getItems();
        foreach ($items as $item) {
            /** @var SearchCriteria $searchCriteria */
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('sku', $item->getSku(), 'eq')
                ->create();

            $store = $this->storeManager->getStore();
            $storeId = $store->getId();
            $currency = $store->getCurrentCurrency()->getCurrencyCode();

            $order->setOrderCurrencyCode($currency);

            /** @var ProductRenderInterface $product */
            $product = current($this->productRenderList->getList($searchCriteria, $storeId, $currency)->getItems());

            $productPrice = $product->getPriceInfo()
                ->getExtensionAttributes()
                ->getTaxAdjustments()
                ->getFinalPrice();
            $item->setPrice($productPrice);
            $itemTotal = $productPrice * $item->getQtyOrdered();
            $item->setRowTotal($itemTotal);

            $productPriceIncludingTax = $product->getPriceInfo()->getFinalPrice();
            $item->setPriceInclTax($productPriceIncludingTax);
            $itemTotalIncluding = $productPriceIncludingTax * $item->getQtyOrdered();

            $this->orderItemRepository->save($item);
            $orderSubtotal += $itemTotal;

            $orderSubtotalIncludingTax += $itemTotalIncluding;
        }

        $order->setSubtotal($orderSubtotal);
        $order->setSubtotalInclTax($orderSubtotalIncludingTax);
        $order->setGrandTotal($orderSubtotalIncludingTax);
        $this->orderRepository->save($order);
    }
}
