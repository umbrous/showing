<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */
declare(strict_types=1);

namespace OutsourcingTeam\Customer\Model\Sales\Data;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Catalog\Helper\Image;
use Magento\Store\Model\StoreManagerInterface;
use OutsourcingTeam\Checkout\Model\HoldedProducts;
use OutsourcingTeam\Customer\Helper\Sales\ImageTable;

class OrderTableItemBuilder
{
    /**
     * @var OrderTableItemFactory
     */
    protected $orderItemFactory;

    /**
     * @var OrderTableItem
     */
    protected $item;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var StockStateInterface
     */
    protected $stockState;
    /**
     * @var Image
     */
    protected $imageHelper;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var ImageTable
     */
    protected $imageTable;
    /**
     * @var HoldedProducts
     */
    protected $holdedProducts;
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * OrderTableItemBuilder constructor.
     * @param OrderTableItemFactory $orderItemFactory
     * @param ProductRepositoryInterface $productRepository
     * @param StockStateInterface $stockState
     * @param Image $imageHelper
     * @param UrlInterface $urlBuilder
     * @param ImageTable $imageTable
     * @param HoldedProducts $holdedProducts
     * @param PriceCurrencyInterface $priceCurrency
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        OrderTableItemFactory $orderItemFactory,
        ProductRepositoryInterface $productRepository,
        StockStateInterface $stockState,
        Image $imageHelper,
        UrlInterface $urlBuilder,
        ImageTable $imageTable,
        HoldedProducts $holdedProducts,
        PriceCurrencyInterface $priceCurrency,
        StoreManagerInterface $storeManager
    ) {
        $this->orderItemFactory = $orderItemFactory;
        $this->productRepository = $productRepository;
        $this->stockState = $stockState;
        $this->imageHelper = $imageHelper;
        $this->urlBuilder = $urlBuilder;
        $this->imageTable = $imageTable;
        $this->holdedProducts = $holdedProducts;
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
    }

    /**
     * Call factory
     */
    protected function initItem()
    {
        $this->item = $this->orderItemFactory->create();
    }

    /**
     * @param OrderItemInterface $item
     * @param string $orderCurrencyCode
     * @return OrderTableItem
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function create(OrderItemInterface $item, string $orderCurrencyCode)
    {
        $this->initItem();

        $product = $this->productRepository->getById($item->getProductId());
        $stock = $this->stockState->getStockQty($item->getProductId());
        $orderId = $item->getOrderId();

        $this->item->setData('product_image', $this->imageTable->getTableProductImage($product));
        $this->item->setData('product_sku', $product->getSku());
        $this->item->setData('product_name', $product->getName());
        $this->item->setData('qty', (int)$item->getQtyOrdered());

        $measureUnits = $product->getCustomAttribute('measure_units');
        $this->item->setData('measure_units', $measureUnits ?
            $measureUnits->getValue() : '');

        $customSku = $product->getCustomAttribute('custom_sku');
        $this->item->setData('custom_sku', $customSku ?
            $customSku->getValue() : '');

        $this->item->setData(
            'product_total_holded_store',
            $this->holdedProducts->getStoreQtyById($item->getProductId())
        );
        $this->item->setData('product_total_store', $stock);
        $this->item->setData(
            'product_price',
            $this->priceCurrency->format(
                $item->getPrice(),
                true,
                PriceCurrencyInterface::DEFAULT_PRECISION,
                $this->storeManager->getStore(),
                $orderCurrencyCode
            )
        );
        $this->item->setData(
            'product_total',
            $this->priceCurrency->format(
                $item->getPrice() * $item->getData('qty_ordered'),
                true,
                PriceCurrencyInterface::DEFAULT_PRECISION,
                $this->storeManager->getStore(),
                $orderCurrencyCode
            )
        );
        $this->item->setData('item_id', $item->getItemId());
        $this->item->setData(
            'cart_update_url',
            $this->urlBuilder->getUrl(
                'client/order/itemQty',
                [
                    'order_id' => $orderId,
                    'item_id' => $item->getItemId()
                ]
            )
        );
        $this->item->setData(
            'cart_delete_url',
            $this->urlBuilder->getUrl(
                'client/order/deleteItem',
                [
                    'order_id' => $orderId,
                    'item_id' => $item->getItemId()
                ]
            )
        );

        return $this->item;
    }
}
