<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Data;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Sales\Api\Data\OrderItemInterfaceFactory;

class OrderItemBuilder
{
    /**
     * @var OrderItemInterfaceFactory
     */
    protected $orderItemInterfaceFactory;

    /**
     * OrderItemBuilder constructor.
     * @param OrderItemInterfaceFactory $orderItemInterfaceFactory
     */
    public function __construct(
        OrderItemInterfaceFactory $orderItemInterfaceFactory
    ) {
        $this->orderItemInterfaceFactory = $orderItemInterfaceFactory;
    }

    /**
     * @param ProductInterface $product
     * @param $qty float
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    public function create(ProductInterface $product, $qty){
        $item = $this->orderItemInterfaceFactory->create();

        $item->setSku($product->getSku());
        $item->setProductId($product->getId());
        $item->setQtyOrdered($qty);
        $item->setPrice($product->getPrice());

        $itemTotal = $product->getPrice() * $qty;
        $item->setRowTotal($itemTotal);
        $item->setProductType($product->getTypeId());

        return $item;
    }
}