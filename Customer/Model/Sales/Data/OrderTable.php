<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */
declare(strict_types=1);

namespace OutsourcingTeam\Customer\Model\Sales\Data;

use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use OutsourcingTeam\Customer\Model\Sales\Order;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;

class OrderTable extends Order
{
    /**
     * @var OrderTableItemBuilder
     */
    protected $itemBuilder;
    /**
     * @var OrderTotalRecounter
     */
    protected $totalRecounter;
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * OrderTable constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param Session $customerSession
     * @param OrderTableItemBuilder $itemBuilder
     * @param OrderTotalRecounter $totalRecounter
     * @param PriceCurrencyInterface $priceCurrency
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        Session $customerSession,
        OrderTableItemBuilder $itemBuilder,
        OrderTotalRecounter $totalRecounter,
        PriceCurrencyInterface $priceCurrency,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($orderRepository, $customerSession);
        $this->itemBuilder = $itemBuilder;
        $this->totalRecounter = $totalRecounter;
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $orderId
     * @return array
     * @throws \Exception
     */
    public function getItemsData($orderId)
    {
        $orderStatus = $this->initOrder($orderId);

        $this->recountOrderTotalIfNeeded($orderStatus);
        $currencyCode = $this->order->getOrderCurrencyCode();

        $items = [];
        foreach ($this->order->getItems() as $item) {
            $tableItem = $this->itemBuilder->create($item, $currencyCode);
            $items[] = $tableItem->toArray();
        }

        $result = [
            'items' => $items,
            'subtotal_excluding_tax' => $this->priceCurrency->format(
                $this->order->getSubtotal(),
                true,
                PriceCurrencyInterface::DEFAULT_PRECISION,
                $this->storeManager->getStore(),
                $currencyCode
            ),
            'subtotal' => $this->priceCurrency->format(
                $this->order->getSubtotalInclTax(),
                true,
                PriceCurrencyInterface::DEFAULT_PRECISION,
                $this->storeManager->getStore(),
                $currencyCode
            )
        ];

        return $result;
    }

    /**
     * @param $orderStatus
     */
    public function recountOrderTotalIfNeeded($orderStatus)
    {
        switch ($orderStatus) {
            case OrderBehaviorStrategy::STATUS_HOLDED:
                $needRecountOrderItemsPrices = true;
                break;
            case OrderBehaviorStrategy::STATUS_SAVED:
                $needRecountOrderItemsPrices = true;
                break;
            default:
                $needRecountOrderItemsPrices = false;
        }

        if ($needRecountOrderItemsPrices) {
            $this->totalRecounter->recount($this->order);
        }
    }
}
