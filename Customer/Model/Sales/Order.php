<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Model\Sales;

use Magento\Customer\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

abstract class Order
{
    /**
     * @var OrderInterface
     */
    protected $order;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * OrderBehaviorStrategy constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param Session $customerSession
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        Session $customerSession
    ) {
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * @param $orderId string|integer
     * @return string
     * @throws \Exception
     */
    public function initOrder($orderId)
    {
        $order = $this->getOrderFromSession($orderId);
        $this->setOrder($order);

        return $this->getOrderStatusCode();
    }

    /**
     * @param OrderInterface $order
     */
    public function setOrder(OrderInterface $order)
    {
        if (!$this->order) {
            $this->order = $order;
        }
    }

    /**
     * @return null|string
     */
    public function getOrderStatusCode(){
        return $this->order->getStatus();
    }

    /**
     * @param $orderId
     * @return OrderInterface
     * @throws NoSuchEntityException
     */
    public function getOrderFromSession($orderId)
    {
        $customerId = $this->customerSession->getCustomerId();

        $order = $this->orderRepository->get($orderId);
        if ($customerId != $order->getCustomerId()) {
            throw new NoSuchEntityException();
        }
        return $order;
    }
}