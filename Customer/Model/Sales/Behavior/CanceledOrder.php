<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Behavior;

use Magento\Framework\Message\Notice;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorInterface;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;

class CanceledOrder extends AbstractOrder implements OrderBehaviorInterface
{
    /**
     * Get class status code
     * @return string
     */
    public function getStatusCode()
    {
        return OrderBehaviorStrategy::STATUS_CANCELED;
    }

    /**
     * Changing order address
     * @param OrderInterface $order
     * @param string $shippingMethod
     * @return void
     */
    public function changeAddress(OrderInterface $order, $shippingMethod = '')
    {
        $this->messageManager->addMessage(new Notice(__('Order address can\'t be changed!')));
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param float $qty
     * @return void
     */
    public function changeItemQty(OrderInterface $order, OrderItemInterface $orderItem, $qty)
    {
        $this->messageManager->addMessage(new Notice(__('Order item qty can\'t be changed!')));
    }

    /**
     * @param OrderInterface $order
     * @param string $sku
     * @param float $qty
     */
    public function addItemToOrderBySku(OrderInterface $order, $sku, $qty = 1.00)
    {
        $this->messageManager->addMessage(new Notice(__('Order item can\'t be added!')));
    }
}