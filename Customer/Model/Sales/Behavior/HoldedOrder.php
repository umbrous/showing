<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Behavior;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Message\Error;
use Magento\Framework\Message\Notice;
use Magento\Framework\Message\Success;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorInterface;

class HoldedOrder extends AbstractOrder implements OrderBehaviorInterface
{
    /**
     * @param OrderInterface $order
     * @return void
     */
    public function process(OrderInterface $order)
    {
        $order->setStatus(OrderBehaviorStrategy::STATUS_PENDING);
        $order->setState(\Magento\Sales\Model\Order::STATE_NEW);
        $this->orderRepository->save($order);

        $this->messageManager->addMessage(new Success(__('Order was successfully placed!')));
    }

    /**
     * @param OrderInterface $order
     * @return void
     */
    public function cancel(OrderInterface $order)
    {
        $order->setStatus(OrderBehaviorStrategy::STATUS_CANCELED);
        $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
        $this->stock->addItemsToStockByOrder($order);
        $this->orderRepository->save($order);

        $this->messageManager->addMessage(new Success(__('Order was canceled!')));
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return OrderBehaviorStrategy::STATUS_HOLDED;
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param float $qty
     * @throws OutOfStockException
     * @return void
     */
    public function changeItemQty(OrderInterface $order, OrderItemInterface $orderItem, $qty)
    {
        if($this->validateItemQty($orderItem, $qty)){
            $diff = parent::changeItemQty($order, $orderItem, $qty);
            $this->stock->changeStockItemOnChangeOrderItemQty($orderItem, $diff);
        } else {
            throw new OutOfStockException();
        }
    }

    /**
     * @param OrderItemInterface $item
     * @param float $qty
     * @return bool
     */
    protected function validateItemQty(OrderItemInterface $item, $qty){
        if($item->getQtyOrdered() < $qty){
            return $this->qtyValidator->checkQtyByItem($item, $qty - $item->getQtyOrdered());
        } else {
            return true;
        }
    }

    /**
     * @param OrderInterface $order
     * @param $qty
     * @param ProductInterface $product
     * @return void
     */
    protected function addProductToOrder(OrderInterface $order, $qty, ProductInterface $product)
    {
        $item = parent::addProductToOrder($order, $qty, $product);
        $this->stock->subtractItem($item);
    }

    /**
     * @param $item
     */
    protected function removeItem($item)
    {
        parent::removeItem($item);
        $this->stock->addItem($item);
    }
}