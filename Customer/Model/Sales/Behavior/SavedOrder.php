<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Behavior;


use Magento\Framework\Message\Error;
use Magento\Framework\Message\Success;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorInterface;

class SavedOrder extends AbstractOrder implements OrderBehaviorInterface
{
    /**
     * @param OrderInterface $order
     * @return void
     */
    public function process(OrderInterface $order)
    {
        if($this->validateOrderItemsQty($order)){
            $order->setStatus(OrderBehaviorStrategy::STATUS_PENDING);
            $order->setState(\Magento\Sales\Model\Order::STATE_NEW);
            $this->stock->subtractItemsFromStockByOrder($order);
            $this->orderRepository->save($order);

            $this->messageManager->addMessage(new Success('Order was successfully placed!'));
        }
    }

    /**
     * @param OrderInterface $order
     * @return void
     */
    public function hold(OrderInterface $order)
    {
        if($this->validateOrderItemsQty($order)){
            $order->setStatus(OrderBehaviorStrategy::STATUS_HOLDED);
            $order->setState(\Magento\Sales\Model\Order::STATE_HOLDED);
            $this->stock->subtractItemsFromStockByOrder($order);
            $this->orderRepository->save($order);
            $this->messageManager->addMessage(new Success(__('Order was successfully reserved!')));
        }
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return OrderBehaviorStrategy::STATUS_SAVED;
    }

    /**
     * @param OrderInterface $order
     * @return bool
     */
    public function validateOrderItemsQty(OrderInterface $order){
        if($this->qtyValidator->checkOrderQtyBeforeStockChange($order))
        {
            return true;
        } else {
            $this->messageManager->addMessage(new Error(__('Requested products out of stock!')));
            return false;
        }
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param float $qty
     * @throws OutOfStockException
     * @return void
     */
    public function changeItemQty(OrderInterface $order, OrderItemInterface $orderItem, $qty)
    {
        if($this->validateItemQty($orderItem, $qty)){
            parent::changeItemQty($order, $orderItem, $qty);
        } else {
            throw new OutOfStockException();
        }
    }

    /**
     * @param OrderItemInterface $item
     * @param float $qty
     * @return boolean
     */
    protected function validateItemQty(OrderItemInterface $item, $qty){
        return $this->qtyValidator->checkQtyByItem($item, $qty);
    }
}