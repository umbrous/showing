<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Behavior;


class OutOfStockException extends \Exception
{
    public function __construct()
    {
        parent::__construct(__('Requested products out of stock!'), 0, null);
    }

}