<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Behavior;


class WrongBehaviorException extends \Exception
{
    public function __construct()
    {
        parent::__construct(__('Wrong order behavior!'), 0, null);
    }

}