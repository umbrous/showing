<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Behavior;


use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Message\Notice;
use Magento\Framework\Message\Success;
use Magento\Framework\Registry;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderAddressRepositoryInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use OutsourcingTeam\Customer\Model\Sales\Address\AddressResolver;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderItemBuilder;
use OutsourcingTeam\Customer\Model\Sales\Inventory\ProductQtyValidator;
use OutsourcingTeam\Customer\Model\Sales\Inventory\StockState;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorInterface;
use Magento\Shipping\Model\Config\Source\Allmethods;
use Magento\Sales\Api\Data\OrderStatusHistoryInterfaceFactory;

abstract class AbstractOrder implements OrderBehaviorInterface
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var StockState
     */
    protected $stock;
    /**
     * @var string
     */
    protected $shippingMethod  = 'freeshipping_freeshipping';
    /**
     * @var AddressResolver
     */
    protected $addressResolver;
    /**
     * @var OrderAddressRepositoryInterface
     */
    protected $orderAddressRepository;
    /**
     * @var Allmethods
     */
    protected $allmethods;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    /**
     * @var OrderStatusHistoryInterfaceFactory
     */
    protected $orderStatusHistoryInterfaceFactory;
    /**
     * @var OrderManagementInterface
     */
    protected $orderManagement;
    /**
     * @var ProductQtyValidator
     */
    protected $qtyValidator;
    /**
     * @var OrderItemRepositoryInterface
     */
    protected $orderItemRepository;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var OrderItemBuilder
     */
    protected $orderItemBuilder;

    /**
     * AbstractOrder constructor.
     * @param ManagerInterface $messageManager
     * @param Registry $registry
     * @param OrderRepositoryInterface $orderRepository
     * @param StockState $stock
     * @param AddressResolver $addressResolver
     * @param OrderAddressRepositoryInterface $orderAddressRepository
     * @param Allmethods $allmethods
     * @param \Magento\Framework\App\RequestInterface $request
     * @param OrderStatusHistoryInterfaceFactory $orderStatusHistoryInterfaceFactory
     * @param OrderManagementInterface $orderManagement
     * @param ProductQtyValidator $qtyValidator
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param ProductRepositoryInterface $productRepository
     * @param OrderItemBuilder $orderItemBuilder
     * @codeCoverageIgnore
     */
    public function __construct(
        ManagerInterface $messageManager,
        Registry $registry,
        OrderRepositoryInterface $orderRepository,
        StockState $stock,
        AddressResolver $addressResolver,
        OrderAddressRepositoryInterface $orderAddressRepository,
        Allmethods $allmethods,
        \Magento\Framework\App\RequestInterface $request,
        OrderStatusHistoryInterfaceFactory $orderStatusHistoryInterfaceFactory,
        OrderManagementInterface $orderManagement,
        ProductQtyValidator $qtyValidator,
        OrderItemRepositoryInterface $orderItemRepository,
        ProductRepositoryInterface $productRepository,
        OrderItemBuilder $orderItemBuilder
    ) {
        $this->messageManager = $messageManager;
        $this->registry = $registry;
        $this->orderRepository = $orderRepository;
        $this->stock = $stock;
        $this->addressResolver = $addressResolver;
        $this->orderAddressRepository = $orderAddressRepository;
        $this->allmethods = $allmethods;
        $this->request = $request;
        $this->orderStatusHistoryInterfaceFactory = $orderStatusHistoryInterfaceFactory;
        $this->orderManagement = $orderManagement;
        $this->qtyValidator = $qtyValidator;
        $this->orderItemRepository = $orderItemRepository;
        $this->productRepository = $productRepository;
        $this->orderItemBuilder = $orderItemBuilder;
    }

    /**
     * Change order status to processing
     * @param OrderInterface $order
     * @return void
     */
    public function process(OrderInterface $order)
    {
        $this->messageManager->addMessage(new Notice(__('Order can\'t be processed!')));
    }

    /**
     * Change order status to holded
     * @param OrderInterface $order
     * @return void
     */
    public function hold(OrderInterface $order)
    {
        $this->messageManager->addMessage(new Notice(__('This order can\'t be holded!')));
    }

    /**
     * Canceling order
     * @param OrderInterface $order
     * @return void
     */
    public function cancel(OrderInterface $order)
    {
        $this->messageManager->addMessage(new Notice(__('Order can\'t be canceled!')));
    }

    /**
     * Deleting order
     * @param OrderInterface $order
     * @return void
     */
    public function delete(OrderInterface $order)
    {
        $this->registry->register('isSecureArea',true);
        $this->orderRepository->delete($order);
        $this->registry->unregister('isSecureArea');
        $this->messageManager->addMessage(new Success(__('Order was successfully deleted!')));
    }

    /**
     * @param OrderInterface $order
     * @param string $shippingMethod
     */
    public function changeAddress(OrderInterface $order, $shippingMethod = '')
    {
        $address = $this->createOrderShippingAddress($order);

        $order->setShippingAddress($address);
        $this->orderAddressRepository->save($address);

        $order->setShippingDescription($this->getShippingDescriptionByShippingMethod($shippingMethod));
        $this->orderRepository->save($order);
    }

    /**
     * @param OrderInterface $order
     * @param string $comment
     */
    public function setComment(OrderInterface $order, $comment)
    {
        $commentInstance = $this->orderStatusHistoryInterfaceFactory->create();
        $commentInstance->setComment($comment);
        $commentInstance->setStatus($order->getStatus());
        $this->orderManagement->addComment($order->getEntityId(), $commentInstance);

        $this->orderRepository->save($order);
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param float $qty
     * @return float|null
     */
    public function changeItemQty(OrderInterface $order, OrderItemInterface $orderItem, $qty)
    {
        $diff = $qty - $orderItem->getQtyOrdered();

        $orderItem->setQtyOrdered($qty);
        $this->orderItemRepository->save($orderItem);

        $this->changeOrderSubTotalByDiff($order, $orderItem, $diff);
        $this->orderRepository->save($order);

        return $diff;
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     */
    public function removeItemFromOrder(OrderInterface $order, OrderItemInterface $orderItem)
    {
        $currentOrderItems = $order->getItems();
        if (count($currentOrderItems) > 1){
            $this->removeItemFromOrderItemsArray($order, $orderItem, $currentOrderItems);
        } else {
            $this->delete($order);
        }
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param $diff
     * @return OrderInterface
     */
    protected function changeOrderSubTotalByDiff(OrderInterface $order, OrderItemInterface $orderItem, $diff)
    {
        $price = $order->getSubtotal() + $orderItem->getPrice() * $diff;
        $order->setSubtotal($price);
        return $order;
    }

    /**
     * @param OrderInterface $order
     * @param string $sku
     * @param float $qty
     */
    public function addItemToOrderBySku(OrderInterface $order, $sku, $qty = 1.00)
    {
        $product = $this->productRepository->get($sku);
        $orderItem = $this->getItemIfItemInOrderBySku($order, $sku);
        if ($orderItem) {
            $this->changeItemQty($order, $orderItem, $orderItem->getQtyOrdered() + $qty);
        } else {
            if ($this->stock->checkQty($product->getId(), $qty)) {
               $this->addProductToOrder($order, $qty, $product);
            }
        }
    }

    /**
     * @param OrderInterface $order
     * @param $sku
     * @return OrderItemInterface|null
     */
    protected function getItemIfItemInOrderBySku(OrderInterface $order, $sku)
    {
        foreach ($order->getItems() as $orderItem) {
            if ($orderItem->getSku() == $sku) {
                return $orderItem;
            }
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     * @param $qty
     * @param $product
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    protected function addProductToOrder(OrderInterface $order, $qty, ProductInterface $product)
    {
        $item = $this->orderItemBuilder->create($product, $qty);

        $order->setSubtotal($order->getSubtotal() + $item->getRowTotal());
        $item->setOrderId($order->getEntityId());
        $this->orderItemRepository->save($item);

        $items = $order->getItems();
        array_push($items, $item);

        $order->setItems($items);
        $this->orderRepository->save($order);
        return $item;
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param $currentOrderItems OrderItemInterface[]
     */
    protected function removeItemFromOrderItemsArray(OrderInterface $order, OrderItemInterface $orderItem, $currentOrderItems)
    {
        $items = [];
        foreach ($currentOrderItems as $item) {
            if ($orderItem->getItemId() != $item->getItemId()) {
                $items[] = $item;
            } else {
                $this->removeItem($item);
                $this->subtractItemTotalFromSubtotalOnDelete($order, $item);
            }
        }

        $order->setItems($items);
        $this->orderRepository->save($order);
    }

    /**
     * @param $item
     */
    protected function removeItem($item)
    {
        $this->orderItemRepository->delete($item);
    }

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $item
     */
    protected function subtractItemTotalFromSubtotalOnDelete(OrderInterface $order, OrderItemInterface $item)
    {
        $price = $order->getSubtotal() - $item->getPrice() * $item->getQtyOrdered();
        $order->setSubtotal($price);
    }

    /**
     * @param OrderInterface $order
     * @return OrderAddressInterface
     */
    public function createOrderShippingAddress(OrderInterface $order)
    {
        $address = $order->getShippingAddress();
        $entityId = $address->getEntityId();
        $addressData = $this->addressResolver->getAddress()->getData();

        $address->setData($addressData);
        $address->setEntityId($entityId);
        $address->setAddressType(\Magento\Quote\Model\Quote\Address::ADDRESS_TYPE_SHIPPING);

        return $address;
    }

    /**
     * @param $shippingMethod
     * @return string
     */
    public function getShippingDescriptionByShippingMethod($shippingMethod)
    {
        if ($shippingMethod) {
            $this->shippingMethod = $shippingMethod;
        }

        $allmethods = $this->allmethods->toOptionArray();

        foreach ($allmethods as $method) {
            if ($method['value']) {
                foreach ($method['value'] as $value) {
                    if ($value['value'] == $this->shippingMethod) {
                        return $value['label'];
                    }
                }
            }
        }

        return '';
    }
}