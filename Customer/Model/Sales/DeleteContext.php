<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\CanceledOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;

class DeleteContext
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehavior;
    /**
     * @var SavedOrder
     */
    protected $savedOrder;
    /**
     * @var CanceledOrder
     */
    protected $canceledOrder;

    /**
     * ProcessContext constructor.
     * @param OrderBehaviorStrategy $orderBehavior
     * @param SavedOrder $savedOrder
     * @param CanceledOrder $canceledOrder
     */
    public function __construct(
        OrderBehaviorStrategy $orderBehavior,
        SavedOrder $savedOrder,
        CanceledOrder $canceledOrder
    ) {
        $this->orderBehavior = $orderBehavior;
        $this->savedOrder = $savedOrder;
        $this->canceledOrder = $canceledOrder;
    }

    /**
     * @param $orderId string|integer
     * @throws WrongBehaviorException
     */
    public function delete($orderId){
       $orderStatus = $this->orderBehavior->initOrder($orderId);

       switch ($orderStatus){
           case OrderBehaviorStrategy::STATUS_CANCELED:
               $this->orderBehavior->setBehavior($this->canceledOrder);
               break;
           case OrderBehaviorStrategy::STATUS_HOLDED:
               $this->orderBehavior->setBehavior($this->canceledOrder);
               break;
           case OrderBehaviorStrategy::STATUS_SAVED:
               $this->orderBehavior->setBehavior($this->savedOrder);
               break;
           default:
               throw new WrongBehaviorException();
       }

       $this->orderBehavior->delete();
    }
}