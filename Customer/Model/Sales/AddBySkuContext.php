<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;

class AddBySkuContext
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehavior;
    /**
     * @var SavedOrder
     */
    protected $savedOrder;
    /**
     * @var HoldedOrder
     */
    protected $holdedOrder;

    /**
     * ProcessContext constructor.
     * @param OrderBehaviorStrategy $orderBehavior
     * @param SavedOrder $savedOrder
     * @param HoldedOrder $holdedOrder
     */
    public function __construct(
        OrderBehaviorStrategy $orderBehavior,
        SavedOrder $savedOrder,
        HoldedOrder $holdedOrder
    ) {
        $this->orderBehavior = $orderBehavior;
        $this->savedOrder = $savedOrder;
        $this->holdedOrder = $holdedOrder;
    }

    /**
     * @param $orderId
     * @param $sku
     * @param $qty
     * @throws WrongBehaviorException
     */
    public function addBySku($orderId, $sku, $qty){
       $orderStatus = $this->orderBehavior->initOrder($orderId);

       switch ($orderStatus){
           case OrderBehaviorStrategy::STATUS_HOLDED:
               $this->orderBehavior->setBehavior($this->holdedOrder);
               break;
           case OrderBehaviorStrategy::STATUS_SAVED:
               $this->orderBehavior->setBehavior($this->savedOrder);
               break;
           default:
               throw new WrongBehaviorException();
       }

       $this->orderBehavior->addBySku($sku, $qty);
    }
}