<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales;


use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

interface OrderBehaviorInterface
{
    /**
     * Change order status to processing
     * @param OrderInterface $order
     * @return void
     */
    public function process(OrderInterface $order);

    /**
     * Change order status to holded
     * @param OrderInterface $order
     * @return void
     */
    public function hold(OrderInterface $order);

    /**
     * Canceling order
     * @param OrderInterface $order
     * @return void
     */
    public function cancel(OrderInterface $order);

    /**
     * Deleting order
     * @param OrderInterface $order
     * @return void
     */
    public function delete(OrderInterface $order);

    /**
     * Changing order address
     * @param OrderInterface $order
     * @param $shippingMethod string
     * @return void
     */
    public function changeAddress(OrderInterface $order, $shippingMethod = '');

    /**
     * Get class status code
     * @return string
     */
    public function getStatusCode();

    /**
     * @param OrderInterface $order
     * @param $comment string
     * @return void
     */
    public function setComment(OrderInterface $order, $comment);

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param $qty float
     * @return float Difference between old qty and new
     */
    public function changeItemQty(OrderInterface $order, OrderItemInterface $orderItem, $qty);

    /**
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @return void
     */
    public function removeItemFromOrder(OrderInterface $order, OrderItemInterface $orderItem);

    /**
     * @param OrderInterface $order
     * @param $sku string
     * @param float $qty
     * @return void
     */
    public function addItemToOrderBySku(OrderInterface $order, $sku, $qty = 1.00);
}