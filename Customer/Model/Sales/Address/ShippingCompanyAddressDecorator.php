<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Address;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Model\Address\Config;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;

class ShippingCompanyAddressDecorator extends \Magento\Customer\Model\Address
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\Eav\Model\Config $eavConfig,
        Config $addressConfig,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        AddressMetadataInterface $metadataService,
        AddressInterfaceFactory $addressDataFactory,
        RegionInterfaceFactory $regionDataFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        CustomerFactory $customerFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataProcessor,
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Framework\App\RequestInterface $request,
        $resource = null,
        $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $directoryData, $eavConfig, $addressConfig, $regionFactory, $countryFactory, $metadataService, $addressDataFactory, $regionDataFactory, $dataObjectHelper, $customerFactory, $dataProcessor, $indexerRegistry, $resource, $resourceCollection, $data);
        $this->request = $request;
    }

    /**
     * Decorator function
     * get customer address by id and return decorated address
     * @param Customer $customer
     * @return $this
     */
    public function setDataByCustomerRequest(Customer $customer)
    {
        $address = $customer->getAddressById($this->request->getParam('shippingPerson'));

        $this->setData($address->getData());

        $this->setStreet($this->request->getParam('warehouse'));
        $this->setData('city', $this->request->getParam('city'));
        $this->setData('region', '');
        $this->setData('postcode', '-------');

        return $this;
    }
}