<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Address;


use OutsourcingTeam\Checkout\Model\Checkout\ShippingType;

class AddressResolver
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    /**
     * @var ShippingCompanyAddressDecorator
     */
    protected $shippingCompanyAddress;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\RequestInterface $request,
        ShippingCompanyAddressDecorator $shippingCompanyShippingAddress
    ) {
        $this->customerSession = $customerSession;
        $this->request = $request;
        $this->shippingCompanyAddress = $shippingCompanyShippingAddress;
    }

    /**
     * @return $this|\Magento\Customer\Model\Address
     */
    public function getAddress(){
        $shippingType = $this->request->getParam('shippingType');
        $customer = $this->customerSession->getCustomer();

        if($shippingType == ShippingType::SHIPPING_COMPANY){
            $address =  $this->shippingCompanyAddress->setDataByCustomerRequest($customer);
        } else {
            $address = $customer->getAddressById($this->request->getParam('shippingAddress'));
        }

        $address->setData('email', $customer->getEmail());

        return $address;
    }
}