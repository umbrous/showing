<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales;


use OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;

class ProcessContext
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehavior;
    /**
     * @var HoldedOrder
     */
    protected $holdedOrder;
    /**
     * @var SavedOrder
     */
    protected $savedOrder;

    /**
     * ProcessContext constructor.
     * @param OrderBehaviorStrategy $orderBehavior
     * @param HoldedOrder $holdedOrder
     * @param SavedOrder $savedOrder
     */
    public function __construct(
        OrderBehaviorStrategy $orderBehavior,
        HoldedOrder $holdedOrder,
        SavedOrder $savedOrder
    ) {
        $this->orderBehavior = $orderBehavior;
        $this->holdedOrder = $holdedOrder;
        $this->savedOrder = $savedOrder;
    }

    /**
     * @param $orderId string|integer
     * @throws WrongBehaviorException
     */
    public function process($orderId){
       $orderStatus = $this->orderBehavior->initOrder($orderId);

       switch ($orderStatus){
           case OrderBehaviorStrategy::STATUS_HOLDED:
               $this->orderBehavior->setBehavior($this->holdedOrder);
               break;
           case OrderBehaviorStrategy::STATUS_SAVED:
               $this->orderBehavior->setBehavior($this->savedOrder);
               break;
           default:
               throw new WrongBehaviorException();
       }

       $this->orderBehavior->process();
    }
}