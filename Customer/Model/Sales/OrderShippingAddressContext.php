<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales;

use OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;

class OrderShippingAddressContext
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehavior;
    /**
     * @var HoldedOrder
     */
    protected $holdedOrder;
    /**
     * @var SavedOrder
     */
    protected $savedOrder;

    /**
     * OrderShippingAddressContext constructor.
     * @param OrderBehaviorStrategy $orderBehavior
     * @param HoldedOrder $holdedOrder
     * @param SavedOrder $savedOrder
     */
    public function __construct(
        OrderBehaviorStrategy $orderBehavior,
        HoldedOrder $holdedOrder,
        SavedOrder $savedOrder
    ) {
        $this->orderBehavior = $orderBehavior;
        $this->holdedOrder = $holdedOrder;
        $this->savedOrder = $savedOrder;
    }

    /**
     * @param $orderId float|int
     * @param $shippingMethod string
     * @param $comment string
     * @throws WrongBehaviorException
     * @return void
     */
    public function change($orderId, $shippingMethod, $comment){
        $orderStatus = $this->orderBehavior->initOrder($orderId);

        switch ($orderStatus){
            case OrderBehaviorStrategy::STATUS_HOLDED:
                $this->orderBehavior->setBehavior($this->holdedOrder);
                break;
            case OrderBehaviorStrategy::STATUS_SAVED:
                $this->orderBehavior->setBehavior($this->savedOrder);
                break;
            default:
                throw new WrongBehaviorException();
        }

        $this->orderBehavior->changeAddress($shippingMethod);

        if($comment){
            $this->orderBehavior->setComment($comment);
        }
    }
}