<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Inventory;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use OutsourcingTeam\Customer\Model\Sales\Behavior\OutOfStockException;

class StockState
{
    /**
     * @var StockStateInterface
     */
    protected $stockState;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * StockState constructor.
     * @param StockStateInterface $stockState
     * @param StockRegistryInterface $stockRegistry
     * @codeCoverageIgnore
     */
    public function __construct(
        StockStateInterface $stockState,
        StockRegistryInterface $stockRegistry
    )
    {
        $this->stockState = $stockState;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @param OrderInterface $order
     * @return void
     */
    public function subtractItemsFromStockByOrder(OrderInterface $order)
    {
        foreach ($order->getItems() as $item) {
            $this->subtractItem($item);
        }
    }

    /**
     * @param OrderInterface $order
     * @return void
     */
    public function addItemsToStockByOrder(OrderInterface $order)
    {
        foreach ($order->getItems() as $item) {
            $this->addItem($item);
        }
    }

    /**
     * @param OrderItemInterface $item
     * @throws OutOfStockException
     */
    public function subtractItem(OrderItemInterface $item)
    {
        $stockItem = $this->getStockItemByOrderItem($item);
        $newQty = $stockItem->getQty() - $item->getQtyOrdered();
        if($newQty < 0){
            throw new OutOfStockException();
        }
        $stockItem->setQty($newQty);
        $this->stockRegistry->updateStockItemBySku($item->getSku(), $stockItem);
    }

    /**
     * @param OrderItemInterface $item
     */
    public function addItem(OrderItemInterface $item)
    {
        $stockItem = $this->getStockItemByOrderItem($item);
        $newQty = $stockItem->getQty() + $item->getQtyOrdered();
        $stockItem->setQty($newQty);
        $stockItem->setIsInStock((bool)$newQty);
        $this->stockRegistry->updateStockItemBySku($item->getSku(), $stockItem);
    }

    /**
     * @param OrderItemInterface $orderItem
     * @param $diff
     */
    public function changeStockItemOnChangeOrderItemQty(OrderItemInterface $orderItem, $diff)
    {
        $stockItem = $this->getStockItemByOrderItem($orderItem);
        $newStockQty = $stockItem->getQty() - $diff;
        $stockItem->setQty($newStockQty);
        $stockItem->setIsInStock((bool)$newStockQty);
        $this->stockRegistry->updateStockItemBySku($orderItem->getSku(), $stockItem);
    }

    /**
     * @param OrderItemInterface $item
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    protected function getStockItemByOrderItem(OrderItemInterface $item)
    {
        return $this->stockRegistry->getStockItemBySku($item->getSku());
    }

    /**
     * @param $productId
     * @param $qty
     * @return bool
     * @codeCoverageIgnore
     */
    public function checkQty($productId, $qty){
        return $this->stockState->checkQty($productId, $qty);
    }
}