<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Model\Sales\Inventory;

use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

class ProductQtyValidator
{
    /**
     * @var StockStateInterface
     */
    protected $stockState;

    /**
     * ProductQtyValidator constructor.
     * @param StockStateInterface $stockState
     * @codeCoverageIgnore
     */
    public function __construct(StockStateInterface $stockState)
    {
        $this->stockState = $stockState;
    }

    /**
     * @param OrderInterface $order
     * @return bool
     */
    public function checkOrderQtyBeforeStockChange(OrderInterface $order){
        foreach ($order->getItems() as $item){
            if(!$this->checkQtyByItem($item, $item->getQtyOrdered())){
                return false;
            };
        }

        return true;
    }

    /**
     * @param OrderItemInterface $item
     * @param $newQty
     * @return bool
     */
    public function checkQtyByItem(OrderItemInterface $item, $newQty){
        return $this->stockState->checkQty($item->getProductId(), $newQty);
    }
}