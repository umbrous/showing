<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Model\Sales;

use Magento\Framework\Exception\NoSuchEntityException;
use OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException;

class OrderBehaviorStrategy extends Order
{
    const STATUS_PENDING = 'pending';

    const STATUS_SAVED = 'saved';

    const STATUS_HOLDED = 'holded';

    const STATUS_CANCELED = 'canceled';

    const STATUS_COMPLETED = 'completed';

    /**
     * @var OrderBehaviorInterface
     */
    protected $behavior;

    /**
     * @param OrderBehaviorInterface $behavior
     * @throws WrongBehaviorException|\LogicException
     */
    public function setBehavior(OrderBehaviorInterface $behavior)
    {
        if (!$this->order) {
            throw new \LogicException(__('Order not set!'));
        }
        if (!$this->isValidBehavior($behavior)) {
            throw new WrongBehaviorException();
        }
        $this->behavior = $behavior;
    }

    /**
     * @param OrderBehaviorInterface $behavior
     * @return bool
     */
    protected function isValidBehavior(OrderBehaviorInterface $behavior)
    {
        return $this->order->getStatus() === $behavior->getStatusCode();
    }

    /**
     * Change order status to processing
     * @return void
     */
    public function process()
    {
        $this->behavior->process($this->order);
    }

    /**
     * Change order status to holded
     * @return void
     */
    public function hold()
    {
        $this->behavior->hold($this->order);
    }

    /**
     * Canceling order
     * @return void
     */
    public function cancel()
    {
        $this->behavior->cancel($this->order);
    }

    /**
     * Deleting order
     * @return void
     */
    public function delete()
    {
        $this->behavior->delete($this->order);
    }

    /**
     * Changing order address
     * @param $shippingMethod string
     * @return void
     */
    public function changeAddress($shippingMethod)
    {
        $this->behavior->changeAddress($this->order, $shippingMethod);
    }

    /**
     * @param $comment
     * @return void
     */
    public function setComment($comment){
        $this->behavior->setComment($this->order, $comment);
    }

    /**
     * Get class status code
     * @return string
     */
    public function getStatusCode()
    {
        return $this->behavior->getStatusCode();
    }

    /**
     * @param $qty float
     * @param $itemId integer
     * @return void
     */
    public function changeQty($qty, $itemId){
        $this->behavior->changeItemQty($this->order, $this->getItem($itemId), $qty);
    }

    /**
     * @param $itemId integer
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     * @throws NoSuchEntityException
     */
    protected function getItem($itemId)
    {
        foreach ($this->order->getItems() as $orderItem) {
            if ($orderItem->getItemId() == $itemId) {
                return $orderItem;
            }
        }

        throw new NoSuchEntityException();
    }

    /**
     * @param $sku string
     * @param $qty float
     * @return void
     */
    public function addBySku($sku, $qty){
        $this->behavior->addItemToOrderBySku($this->order, $sku, $qty);
    }

    /**
     * @param $itemId
     */
    public function removeItem($itemId){
        $this->behavior->removeItemFromOrder($this->order, $this->getItem($itemId));
    }
}