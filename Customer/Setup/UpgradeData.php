<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Setup;


use Magento\Customer\Model\Address;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    /**
     * @var AttributeSetFactory
     */
    private $setFactory;
    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $setFactory,
        AttributeRepositoryInterface $attributeRepository
    ) {

        $this->customerSetupFactory = $customerSetupFactory;
        $this->setFactory = $setFactory;
        $this->attributeRepository = $attributeRepository;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '1.0.0.5', '<')) {
            $attributeParams = [
                'type' => 'varchar',
                'label' => 'Facebook Page',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'visible_on_front' => true,
                'user_defined' => false,
                'sort_order' => 43,
                'position' => 43,
                'system' => 0,
            ];

            $customerSetup->addAttribute('customer_address', 'facebook_page', $attributeParams);

            $faceboolAttribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'facebook_page');
            $faceboolAttribute->setData(
                'used_in_forms',
                ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address','customer_address']
            );
            $faceboolAttribute->save();
        }

        if (version_compare($context->getVersion(), '1.0.1.1', '<')) {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $setup->startSetup();
            $attributesInfo = [
                'products_per_page' => [
                    'type' => 'int',
                    'label' => 'Products Per Page',
                    'input' => 'select',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                    'required' => false,
                    'sort_order' => 111,
                    'visible' => false,
                    'system' => false,
                    'validate_rules' => '',
                    'position' => 1001,
                    'admin_checkout' => 1,
                    'option' => ['values' => ['20', '40', '100']],
                ],
                'additional_price_fee' => [
                    'label' => 'Additional Price Fee',
                    'type' => 'int',
                    'input' => 'text',
                    'position' => 1002,
                    'visible' => true,
                    'required' => false,
                    'system' => 0,
                    'user_defined' => true,
                    'class' => 'validate-number'
                ],
                'show_search_tips' => [
                    'label' => 'Show Search Tips',
                    'type' => 'text',
                    'input' => 'boolean',
                    'position' => 1002,
                    'visible' => true,
                    'required' => false,
                    'system' => 0,
                    'user_defined' => true
                ],
                'show_product_image' => [
                    'label' => 'Show Product Image',
                    'type' => 'text',
                    'input' => 'boolean',
                    'position' => 1003,
                    'visible' => true,
                    'required' => false,
                    'system' => 0,
                    'user_defined' => true
                ],
                'show_retail_price' => [
                    'label' => 'Show Retail Price',
                    'type' => 'text',
                    'input' => 'boolean',
                    'position' => 1004,
                    'visible' => true,
                    'required' => false,
                    'system' => 0,
                    'user_defined' => true
                ],
                'show_own_price' => [
                    'label' => 'Show Own Price',
                    'type' => 'text',
                    'input' => 'boolean',
                    'position' => 1003,
                    'visible' => true,
                    'required' => false,
                    'system' => 0,
                    'user_defined' => true
                ]
            ];
            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->setFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            foreach ($attributesInfo as $attributeCode => $attributeParams) {
                $customerSetup->addAttribute(Customer::ENTITY, $attributeCode, $attributeParams);
                $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $attributeCode);
                $attribute->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer'],
                ]);

                $attribute->save();
            }

        }

        $setup->endSetup();
    }

}