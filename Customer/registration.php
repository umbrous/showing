<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'OutsourcingTeam_Customer',
    __DIR__
);