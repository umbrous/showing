<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */
declare(strict_types=1);

namespace OutsourcingTeam\Customer\Block;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Framework\View\Element\Template;

class Customer extends \Magento\Framework\View\Element\Template
{
    protected $customerSession;

    protected $formkey;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    protected $customer;
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * Customer constructor.
     * @param Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Data\Form\FormKey $formKey,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Eav\Model\Config $eavConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->formkey = $formKey;
        $this->customerRepository = $customerRepository;
        $this->customer = $this->customerSession->getCustomer();
        $this->eavConfig = $eavConfig;
    }

    /**
     * Retrieve customer object
     *
     * @return CustomerModel
     */
    public function getCustomer()
    {
        return $this->customerSession->getCustomer();
    }

    /**
     * Retrieve form key
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getFormKey()
    {
        return $this->formkey->getFormKey();
    }

    /**
     * @param $attribute
     * @return bool|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomAttribute($attribute)
    {
        $customAttribute = '';
        $customerId = $this->customerSession->getCustomerId();
        if ($customerId) {
            $customAttribute = $this->customerRepository
                ->getById($customerId)
                ->getCustomAttribute($attribute);
        }
        if ($customAttribute) {
            return $customAttribute->getValue();
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getProductsPerPageSelect()
    {
        $productsPerPage = $this->customer->getAttribute('products_per_page');

        $options = $productsPerPage->getSource()->getAllOptions();
        array_shift($options);
        return $options;
    }

    /**
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductsPerPage()
    {
        return $this->eavConfig->getAttribute('customer', 'products_per_page')
            ->getSource()->getOptionText($this->getCustomAttribute('products_per_page'));
    }

    /**
     * @return bool|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getShowProductImage()
    {
        return $this->getCustomAttribute('show_product_image');
    }
}
