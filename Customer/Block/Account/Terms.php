<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Block\Account;

use OutsourcingTeam\Customer\Block\Customer;

class Terms extends Customer
{
    public function getUpdateCustomerUrl()
    {
        return $this->getUrl(
            'customer/account/editPost/'
        );
    }

}