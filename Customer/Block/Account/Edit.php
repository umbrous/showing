<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Block\Account;


class Edit extends DefaultSetting
{
    public function getUpdateCustomerUrl()
    {
        return $this->getUrl(
            'customer/account/editPost/'
        );
    }

}