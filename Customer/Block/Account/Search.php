<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */
declare(strict_types=1);

namespace OutsourcingTeam\Customer\Block\Account;

use OutsourcingTeam\Customer\Block\Customer;

class Search extends Customer
{
    /**
     * @return bool|mixed|string
     */
    public function getProductsPerPage()
    {
        return $this->getCustomAttribute('products_per_page');
    }

    /**
     * @return bool|mixed
     */
    public function getShowSearchTips()
    {
        return $this->getCustomAttribute('show_search_tips');
    }

    /**
     * @return bool|mixed
     */
    public function getShowRetailPrice()
    {
        return $this->getCustomAttribute('show_retail_price');
    }

    /**
     * @return bool|mixed
     */
    public function getAdditionalPriceFee()
    {
        return $this->getCustomAttribute('additional_price_fee');
    }

    /**
     * @return bool|mixed
     */
    public function getShowOwnPrice()
    {
        return $this->getCustomAttribute('show_own_price');
    }

    /**
     * @return string
     */
    public function getSearchPostUrl()
    {
        return $this->getUrl(
            'client/setting/searchPost/'
        );
    }
}