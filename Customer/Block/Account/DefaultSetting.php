<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Block\Account;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Magento\Shipping\Model\Config\Source\Allmethods;
use OutsourcingTeam\Customer\Block\Customer;

class DefaultSetting extends Customer
{

    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    protected $allmethods;
    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * DefaultSetting constructor.
     * @param Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param Allmethods $allmethods
     * @param AddressRepositoryInterface $addressRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Data\Form\FormKey $formKey,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Shipping\Model\Config\Source\Allmethods $allmethods,
        AddressRepositoryInterface $addressRepository,
        array $data = []
    ) {
        parent::__construct($context, $customerSession, $formKey, $customerRepository, $eavConfig, $data);
        $this->allmethods = $allmethods;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @return array
     */
    public function getAllMethods(){
        $methods = $this->allmethods->toOptionArray(true);
        if(isset($methods['freeshipping'])){
            unset($methods['freeshipping']);
        }

        return $methods;
    }

    public function getCustomerAddresses(){
        $customerId = $this->customer->getId();
        try {
            $addresses = $this->customerRepository->getById($customerId)->getAddresses();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }

        return $addresses;
    }

    public function getShippingTypes()
    {
        $shippigType = $this->customer->getAttribute('shipping_type');

        $options = $shippigType->getSource()->getAllOptions();
        array_shift($options);
        return $options;
    }

    public function getUpdateUrl()
    {
        return $this->getUrl(
            'client/setting/save/'
        );
    }

    public function getShippingType(){
        return $this->getCustomAttribute('shipping_type');
    }

    public function getDefaultShippingMethod(){
        return $this->getCustomAttribute('default_shipping_method');
    }

    public function getDefaultShippingCity(){
        return $this->getCustomAttribute('default_shipping_city');
    }

    public function getDefaultShippingWarehouse(){
        return $this->getCustomAttribute('default_shipping_warehouse');
    }

    public function getDefaultShipping(){
        $customerId = $this->customer->getId();
        try {
            $defaultShipping = $this->customerRepository->getById($customerId)->getDefaultShipping();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }

        try {
            $defaultShipping = $this->addressRepository->getById($defaultShipping);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }

        return $defaultShipping;
    }

}