<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Block\Account;

use OutsourcingTeam\Customer\Block\Customer;

class Menu extends Customer
{
    public function getAccountUrl()
    {
        return $this->getUrl(
            'customer/account/index'
        );
    }

    public function getEditUrl()
    {
        return $this->getUrl(
            'customer/account/edit'
        );
    }

    public function getAddressUrl()
    {
        return $this->getUrl(
            'customer/address/index'
        );
    }


    public function getSearchSettingUrl()
    {
        return $this->getUrl(
            'client/setting/search'
        );
    }

    public function getCurrentUrl(){
        return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
    }
}