<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Controller\Order;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use OutsourcingTeam\Customer\Model\Sales\Behavior\SavedOrder;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;

class Hold extends Action
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehavior;
    /**
     * @var SavedOrder
     */
    protected $savedOrder;

    public function __construct(
        Context $context,
        OrderBehaviorStrategy $orderBehavior,
        SavedOrder $savedOrder
    ) {
        parent::__construct($context);

        $this->orderBehavior = $orderBehavior;
        $this->savedOrder = $savedOrder;
    }

    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');

        if($orderId){
            $this->orderBehavior->initOrder($orderId);
            $this->orderBehavior->setBehavior($this->savedOrder);
            $this->orderBehavior->hold();
            return $this->resultRedirectFactory->create()
                ->setPath('sales/order/view', ['order_id' => $orderId]);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }

}