<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use OutsourcingTeam\Customer\Model\Sales\DeleteContext;

class Delete extends Action
{
    /**
     * @var DeleteContext
     */
    protected $deleteOrder;

    /**
     * Hide constructor.
     * @param Context $context
     * @param DeleteContext $deleteOrder
     */
    public function __construct(
        Context $context,
        DeleteContext $deleteOrder
    ) {
        parent::__construct($context);
        $this->deleteOrder = $deleteOrder;
    }


    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');

        if($orderId){
            $this->deleteOrder->delete($orderId);
            return $this->resultRedirectFactory->create()->setPath('sales/order/history');
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}