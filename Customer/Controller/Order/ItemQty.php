<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Controller\Order;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use OutsourcingTeam\Customer\Model\Sales\ChangeQtyContext;

class ItemQty extends Action
{
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var ChangeQtyContext
     */
    protected $changeQtyContext;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        ChangeQtyContext $changeQtyContext
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->changeQtyContext = $changeQtyContext;
    }


    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');
        $itemId = $this->_request->getParam('item_id');
        $newQty = $this->_request->getParam('new_qty');

        $json = $this->jsonFactory->create();

        if($orderId && $itemId && $newQty){
            try{
                $this->changeQtyContext->changeQty($orderId, $itemId, $newQty);
            } catch (\Exception $exception){
                $result = ['type'=> 'error', 'message' => $exception->getMessage()];
                return $json->setData($result);
            }

            $result = ['type'=> 'success', 'message' => __('Item quantity successfully changed!')];

            return $json->setData($result);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}