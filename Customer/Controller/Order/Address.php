<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use OutsourcingTeam\Customer\Model\Sales\OrderShippingAddressContext;

class Address extends Action
{
    /**
     * @var OrderShippingAddressContext
     */
    protected $shippingDescription;

    /**
     * Address constructor.
     * @param Context $context
     * @param OrderShippingAddressContext $shippingAddress
     */
    public function __construct(
        Context $context,
        OrderShippingAddressContext $shippingAddress
    ) {
        parent::__construct($context);
        $this->shippingDescription = $shippingAddress;
    }
    
    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \OutsourcingTeam\Customer\Model\Sales\Behavior\WrongBehaviorException
     */
    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');
        $comment = $this->_request->getParam('comment');
        $shippingMethod = $this->_request->getParam('shippingMethod');

        if ($orderId) {
            $this->shippingDescription->change($orderId, $shippingMethod, $comment);
            return $this->resultRedirectFactory->create()->setPath('sales/order/view', ['order_id' => $orderId]);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}
