<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use OutsourcingTeam\Customer\Model\Sales\AddBySkuContext;

class AddItem extends Action
{
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var AddBySkuContext
     */
    protected $addBySkuContext;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        AddBySkuContext $addBySkuContext
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->addBySkuContext = $addBySkuContext;
    }


    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');
        $sku = $this->_request->getParam('sku');
        $qty = $this->_request->getParam('qty');

        $json = $this->jsonFactory->create();

        if($orderId && $qty){
            try{
                $this->addBySkuContext->addBySku($orderId, $sku, $qty);
            } catch (\Exception $exception){
                $result = ['type'=> 'error', 'message' => $exception->getMessage()];
                return $json->setData($result);
            }

            $result = ['type'=> 'success', 'message' => __('Item successfully added!')];

            return $json->setData($result);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}