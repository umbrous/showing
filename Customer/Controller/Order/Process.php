<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Controller\Order;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Process extends Action
{
    /**
     * @var \OutsourcingTeam\Customer\Model\Sales\ProcessContext
     */
    protected $process;

    public function __construct(
        Context $context,
        \OutsourcingTeam\Customer\Model\Sales\ProcessContext $process
    ) {
        parent::__construct($context);
        $this->process = $process;
    }

    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');

        if($orderId){
            $this->process->process($orderId);
            return $this->resultRedirectFactory->create()
                ->setPath('sales/order/view', ['order_id' => $orderId]);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }

}