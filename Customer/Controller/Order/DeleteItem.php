<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */

namespace OutsourcingTeam\Customer\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use OutsourcingTeam\Customer\Model\Sales\Inventory\OrderItemManager;
use OutsourcingTeam\Customer\Model\Sales\RemoveItemContext;

class DeleteItem extends Action
{
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var RemoveItemContext
     */
    protected $removeItemContext;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        RemoveItemContext $removeItemContext
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->removeItemContext = $removeItemContext;
    }


    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');
        $itemId = $this->_request->getParam('item_id');

        $json = $this->jsonFactory->create();

        if($orderId && $itemId){
            try{
                $this->removeItemContext->removeItem($orderId, $itemId);
            } catch (\Exception $exception){
                $result = ['type'=> 'error', 'message' => $exception->getMessage()];
                return $json->setData($result);
            }

            $result = ['type'=> 'success', 'message' => __('Item successfully removed!')];
            return $json->setData($result);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}