<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Controller\Order;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use OutsourcingTeam\Customer\Model\Sales\Behavior\HoldedOrder;
use OutsourcingTeam\Customer\Model\Sales\OrderBehaviorStrategy;

class Cancel extends Action
{
    /**
     * @var OrderBehaviorStrategy
     */
    protected $orderBehavior;
    /**
     * @var HoldedOrder
     */
    protected $holdedOrder;

    public function __construct(
        Context $context,
        OrderBehaviorStrategy $orderBehavior,
        HoldedOrder $holdedOrder
    ) {
        parent::__construct($context);
        $this->orderBehavior = $orderBehavior;
        $this->holdedOrder = $holdedOrder;
    }

    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');

        if($orderId){
            $this->orderBehavior->initOrder($orderId);
            $this->orderBehavior->setBehavior($this->holdedOrder);
            $this->orderBehavior->cancel();
            return $this->resultRedirectFactory->create()
                ->setPath('sales/order/view', ['order_id' => $orderId]);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}