<?php
/**
 * @author outsourcing.team <development5.oteam@gmail.com>
 */

namespace OutsourcingTeam\Customer\Controller\Order;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Quote\Api\CartRepositoryInterface;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTable;
use OutsourcingTeam\Customer\Model\Sales\Order;

class Reorder extends Action
{
    /**
     * @var Order
     */
    protected $order;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        OrderTable $order,
        Session $session,
        ProductRepositoryInterface $productRepository,
        CartRepositoryInterface $cartRepository,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->order = $order;
        $this->session = $session;
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $order = $this->order->getOrderFromSession($this->_request->getParam('order_id'));

        $new = json_decode($this->_request->getParam('new'));

        $quote = $this->session->getQuote();

        if($new){
            $quote->removeAllItems();
        }

        $resultJson = $this->resultJsonFactory->create();

        $items = $order->getItems();
        foreach ($items as $item) {
            try {
                $product = $this->productRepository->getById($item->getProductId());
                $quote->addProduct($product, $item->getQtyOrdered());
                $this->cartRepository->save($quote);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_objectManager->get(\Magento\Checkout\Model\Session::class)->getUseNotice(true)) {
                    $notice = $e->getMessage();
                } else {
                    $notice = $e->getMessage();
                }
                $result = ['type' => 'notice', 'mess' => $notice];
                return $resultJson->setData($result);
            } catch (\Exception $e) {
                $notice = __('We can\'t add this item to your shopping cart right now.');
                $result = ['type' => 'notice', 'mess' => $notice];
                return $resultJson->setData($result);
            }
        }

        $result = ['type' => 'success', 'mess' =>  __('Items successfully added to order!')];

        return $resultJson->setData($result);
    }
}