<?php
/**
 * @author outsourcing.team
 * @email development5.oteam@gmail.com
 */
declare(strict_types=1);

namespace OutsourcingTeam\Customer\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use OutsourcingTeam\Customer\Model\Sales\Data\OrderTable;

class Cart extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var OrderTable
     */
    protected $order;

    /**
     * Cart constructor.
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param OrderTable $order
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        OrderTable $order
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->order = $order;
    }


    public function execute()
    {
        $orderId = $this->_request->getParam('order_id');

        if ($orderId) {
            $itemsData = $this->order->getItemsData($orderId);
            return $this->resultJsonFactory->create()->setData($itemsData);
        } else {
            return $this->resultRedirectFactory->create()->setPath('cms/index/index');
        }
    }
}
